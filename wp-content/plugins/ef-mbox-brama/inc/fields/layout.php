<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'efmb_Layout_Field' ) )
{
	class efmb_Layout_Field
	{
		/**
		 * Enqueue script
		 *
		 * @return void
		 */
		static function admin_enqueue_scripts()	{

			wp_enqueue_style( 'efmb-layout-css', efmb_CSS_URL . 'layout.css', 'style', efmb_VER );
			wp_enqueue_script( 'efmb-layout', efmb_JS_URL . 'layout.js', array( 'jquery' ), efmb_VER, true );
		}

		/**
		 * Get field HTML
		 *
		 * @param string $html
		 * @param mixed  $meta
		 * @param array  $field
		 *
		 * @return string
		 */
		static function html( $html, $meta, $field )
		{

			$html = '';

			$tpl = '<div class="efmb-layout"><input type="radio" class="efmb-radio" name="%s" value="%s" %s /><img src="%s" alt="" /><p>%s</p></div>';

			foreach ( $field['options'] as $value => $label )
			{
				$html .= sprintf(
					$tpl,
					$field['field_name'],
					$value,
					checked( $value, $meta, false ),
					$label[0],
					$label[1]
				);
			}

			return $html;
		}
	}
}