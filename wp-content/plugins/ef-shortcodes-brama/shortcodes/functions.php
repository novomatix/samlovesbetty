<?php

/**
 * Textdomain
 *
 * @since 1.0
 */
function ef_shtd_load_textdomain() {
	$locale = get_locale();
	$dir    = trailingslashit( SHTD_DIR . 'lang' );
	$mofile = "{$dir}{$locale}.mo";
	load_textdomain( EF_SHTD, $mofile );
}
add_action( 'plugins_loaded', 'ef_shtd_load_textdomain' );

/**
 * Required scripts/css
 *
 * @since 1.0.0
 */
function ef_shtd_enqueue_style() {
	wp_enqueue_style( 'jQueryUI', SHTD_JS_URL . 'jquery/jQueryUI/jquery-ui-1.10.2.custom.min.css', array(), SHTD_VERSION, 'screen' );
	wp_enqueue_style( 'ef-shortcodes', SHTD_ADM_URL . 'css/shortcodes.min.css', array(), SHTD_VERSION, 'screen' );

    if ( !defined( EF_TDM ) ) {
    	wp_enqueue_style( 'ef-bootstrap', SHTD_ADM_URL . 'css/bootstrap.min.css', array(), SHTD_VERSION, 'screen' );
    }
}

function ef_shtd_enqueue_script() {

	global $wp_scripts;

	if ( !isset( $wp_scripts->registered[ 'jquery' ] ) ) {
		wp_enqueue_script( 'jquery' );
	}

	if ( !isset( $wp_scripts->registered[ 'ef-easing' ] ) ) {
		wp_enqueue_script( 'ef-easing', SHTD_JS_URL . 'jquery/jquery.easing.1.3.min.js', array( 'jquery' ), SHTD_VERSION, true );
	}

	if ( !isset( $wp_scripts->registered[ 'ef-imagesloaded' ] ) ) {
		wp_enqueue_script( 'ef-imagesloaded', SHTD_JS_URL . 'jquery/imagesloaded.min.js', array( 'jquery' ), SHTD_VERSION, true );
	}

	if ( !isset( $wp_scripts->registered[ 'carouFredSel' ] ) ) {
		wp_enqueue_script( 'carouFredSel', SHTD_JS_URL . 'jquery/carouFredSel/jquery.carouFredSel-6.2.1-packed.js',array('jquery'),SHTD_VERSION,true );
	}

	wp_enqueue_script( 'ef-inview', SHTD_JS_URL . 'jquery/inview.min.js', array( 'jquery' ), SHTD_VERSION, true );

	wp_enqueue_script( 'shtd-carouFredSel-init', SHTD_JS_URL . 'jquery/carouFredSel/carouFredSel.init.js',array('jquery', 'carouFredSel'),SHTD_VERSION,true );
	wp_enqueue_script( 'jquery-ui-tabs' );
	wp_enqueue_script( 'jquery-ui-accordion' );

	wp_enqueue_script( 'shtd-custom', SHTD_JS_URL . 'jquery/init.js','',SHTD_VERSION,true );
}

add_action( 'wp_enqueue_scripts', 'ef_shtd_enqueue_style' );
add_action( 'wp_enqueue_scripts', 'ef_shtd_enqueue_script' );