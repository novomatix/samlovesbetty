(function() {
    "use strict";

    jQuery.fn.ef_validate_fields = function() {
        var this_ = jQuery(this),
            validateField = this_.find('*[class^="mce-validate"]'),
            myRegExp,
            error = 0;

        validateField.each(function(){
            var thisField = jQuery(this);

            if (thisField.attr('class').indexOf('[url]') > -1) {
                myRegExp = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
            } else if (thisField.attr('class').indexOf('[not_empty]') > -1) {
                myRegExp = /\S/;
            } else if (thisField.attr('class').indexOf('[digit]') > -1) {
                myRegExp = /^[1-9][0-9]*$/;
            }

            if (!myRegExp.test(thisField.val())) {
                thisField.focus().addClass('ef-not-valid-field');
                error++;
            } else {
                thisField.removeClass('ef-not-valid-field');
            }
        });

        if (error === 0) {
            return true;
        } else {
            return false;
        }
    };

    tinymce.PluginManager.add( 'ef_shortcodes_button', function( editor, url ) {
        editor.addButton( 'ef_shortcodes_button', {
            type: 'menubutton',
            text: editor.getLang('ef_shtd_lang.shtd_title'),
            icon: 'icon ef-shortcodes-icon',
            menu: [

                /** Formatting **/
                {
                    text: editor.getLang('ef_shtd_lang.shtd_section1'),
                    menu: [

                        /* Headings */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_headings'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 160,
                                    title: editor.getLang('ef_shtd_lang.shtd_headings'),
                                    body: [

                                    // Heading Text
                                    {
                                        type: 'textbox',
                                        name: 'heading_text',
                                        label: editor.getLang('ef_shtd_lang.field_text'),
                                        classes: 'validate[not_empty]',
                                        value: 'This is heading'
                                    },

                                    // Heading Size
                                    {
                                        type: 'listbox',
                                        name: 'heading_size',
                                        label: editor.getLang('ef_shtd_lang.field_size'),
                                        'values': [
                                            {text: 'H1', value: 'h1'},
                                            {text: 'H2', value: 'h2'},
                                            {text: 'H3', value: 'h3'},
                                            {text: 'H4', value: 'h4'},
                                            {text: 'H5', value: 'h5'},
                                            {text: 'H6', value: 'h6'}
                                        ]
                                    } ],
                                    onsubmit: function(e) {
                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-heading size="' + e.data.heading_size + '"]' + e.data.heading_text + '[/ef-heading]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        }, // End Headings


                        /* Dividers */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_dividers'),
                            onclick: function() {
                                editor.windowManager.open( {
                                    width: 450,
                                    height: 160,
                                    title: editor.getLang('ef_shtd_lang.shtd_dividers'),
                                    body: [

                                    // Divider type
                                    {
                                        type: 'listbox',
                                        name: 'divider_type',
                                        label: editor.getLang('ef_shtd_lang.field_type'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.divider_type1'), value: ''},
                                            {text: editor.getLang('ef_shtd_lang.divider_type2'), value: 'ef-blank'}
                                        ]
                                    },

                                    // Divider type
                                    {
                                        type: 'listbox',
                                        name: 'divider_style',
                                        label: editor.getLang('ef_shtd_lang.field_style'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.divider_style1'), value: ''},
                                            {text: editor.getLang('ef_shtd_lang.divider_style2'), value: 'ef-double-divider'},
                                            {text: editor.getLang('ef_shtd_lang.divider_style3'), value: 'ef-dotted-divider'},
                                            {text: editor.getLang('ef_shtd_lang.divider_style4'), value: 'ef-deco1-divider'},
                                            {text: editor.getLang('ef_shtd_lang.divider_style5'), value: 'ef-deco2-divider'},
                                            {text: editor.getLang('ef_shtd_lang.divider_style6'), value: 'ef-deco3-divider'},
                                            {text: editor.getLang('ef_shtd_lang.divider_style7'), value: 'ef-deco4-divider'},
                                            {text: editor.getLang('ef_shtd_lang.divider_style8'), value: 'ef-deco5-divider'}
                                        ]
                                    },

                                    // Divider indent
                                    {
                                        type: 'listbox',
                                        name: 'divider_size',
                                        label: editor.getLang('ef_shtd_lang.divider_bottom'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.divider_type1'), value: ''},
                                            {text: '0px', value: 'ef-bottom-0'},
                                            {text: '20px', value: 'ef-bottom-20'},
                                            {text: '40px', value: 'ef-bottom-40'},
                                            {text: '60px', value: 'ef-bottom-60'},
                                            {text: '80px', value: 'ef-bottom-80'}
                                        ]
                                    } ],
                                    onsubmit: function(e) {
                                        editor.insertContent('[ef-hr style="' + e.data.divider_style + '" type="' + e.data.divider_type + '" class="' + e.data.divider_size + '"][/ef-hr]');
                                    }
                                });
                            }
                        }, // End Dividers

                        /* Columns */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_columns'),
                            onclick: function() {
                                editor.windowManager.open({
                                    width: 480,
                                    height: 100,
                                    title: editor.getLang('ef_shtd_lang.shtd_columns'),
                                    body: [

                                    // Column Size
                                    {
                                        type: 'listbox',
                                        name: 'column_size',
                                        label: editor.getLang('ef_shtd_lang.shtd_columns'),
                                        'values': [
                                            {text: '[6/12] + [6/12]', value: '6'},
                                            {text: '[3/12] + [9/12]', value: '39'},
                                            {text: '[9/12] + [3/12]', value: '93'},
                                            {text: '[4/12] + [4/12] + [4/12]', value: '4'},
                                            {text: '[3/12] + [3/12] + [6/12]', value: '336'},
                                            {text: '[6/12] + [3/12] + [3/12]', value: '633'},
                                            {text: '[3/12] + [6/12] + [3/12]', value: '363'},
                                            {text: '[3/12] + [3/12] + [3/12] + [3/12]', value: '3'},
                                            {text: '[2/12] + [10/12]', value: '210'},
                                            {text: '[2/12] + [8/12] + [2/12]', value: '282'},
                                            {text: '[2/12] + [2/12] + [2/12] + [2/12] + [2/12] + [2/12]', value: '2'}
                                        ]
                                    }],
                                    onsubmit: function( e ) {
                                        var contnt = '<p>Your content goes here</p>',
                                            col = '[ef-columns part="' + e.data.column_size[0] + '"]' + contnt + '[/ef-columns]',
                                            col2 = '[ef-columns part="' + e.data.column_size[1] + '"]' + contnt + '[/ef-columns]',
                                            col3 = '[ef-columns part="' + e.data.column_size[2] + '"]' + contnt + '[/ef-columns]',
                                            col4 = '[ef-columns part="10"]' + contnt + '[/ef-columns]',
                                            column;

                                        if ( e.data.column_size == '6' ) {
                                            column = col + col;
                                        } else if ( e.data.column_size == '4' ) {
                                            column = col + col + col;
                                        } else if ( e.data.column_size == '3' ) {
                                            column = col + col + col + col;
                                        } else if ( e.data.column_size == '2' ) {
                                            column = col + col + col + col + col + col;
                                        } else if ( e.data.column_size == '39' || e.data.column_size == '93' ) {
                                            column = col + col2;
                                        } else if ( e.data.column_size == '210' ) {
                                            column = col + col4;
                                        } else if ( e.data.column_size == '336' || e.data.column_size == '633' || e.data.column_size == '363' || e.data.column_size == '282' ) {
                                            column = col + col2 + col3;
                                        }

                                        editor.insertContent('[ef-row]<br />' + column + '<br />[/ef-row]');
                                    }
                                });
                            }
                        } // End columns
                    ]
                }, // End Formatting Section


                /** Elements section **/
                {
                text: editor.getLang('ef_shtd_lang.shtd_section2'),
                menu: [

                        /* Buttons */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_buttons'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 260,
                                    title: editor.getLang('ef_shtd_lang.shtd_buttons'),
                                    body: [

                                    // Button url
                                    {
                                        type: 'textbox',
                                        name: 'btn_url',
                                        label: editor.getLang('ef_shtd_lang.field_url'),
                                        classes: 'validate[url]',
                                        value: 'http://'
                                    },

                                    // Button text
                                    {
                                        type: 'textbox',
                                        name: 'btn_text',
                                        label: editor.getLang('ef_shtd_lang.field_text'),
                                        classes: 'validate[not_empty]',
                                        value: 'Button text'
                                    },

                                    // Button type
                                    {
                                        type: 'listbox',
                                        name: 'btn_type',
                                        label: editor.getLang('ef_shtd_lang.field_type'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.btn_type_val1'), value: 'btn-primary'},
                                            {text: editor.getLang('ef_shtd_lang.divider_type1'), value: 'btn-default'}
                                        ]
                                    },

                                    // Button size
                                    {
                                        type: 'listbox',
                                        name: 'btn_size',
                                        label: editor.getLang('ef_shtd_lang.field_size'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.btn_size1'), value: 'btn-xs'},
                                            {text: editor.getLang('ef_shtd_lang.btn_size2'), value: 'btn-sm'},
                                            {text: editor.getLang('ef_shtd_lang.btn_size3'), value: 'btn-lg'}
                                        ]
                                    },

                                    // Button target
                                    {
                                        type: 'textbox',
                                        name: 'btn_target',
                                        label: editor.getLang('ef_shtd_lang.btn_target'),
                                        value: '_blank'
                                    }],

                                    onsubmit: function(e) {
                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-button class="btn' + ' ' + e.data.btn_size + ' ' + e.data.btn_type + '" href="' + e.data.btn_url + '" title="' + e.data.btn_text + '" target="' + e.data.btn_target + '"]' + e.data.btn_text + '[/ef-button]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        }, // End Buttons

                        /* Panels */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_panels'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 300,
                                    title: editor.getLang('ef_shtd_lang.shtd_panels'),
                                    body: [

                                    // Panel type
                                    {
                                        type: 'listbox',
                                        name: 'panel_type',
                                        label: editor.getLang('ef_shtd_lang.field_type'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.divider_type1'), value: 'panel-default'},
                                            {text: editor.getLang('ef_shtd_lang.field_danger'), value: 'panel-danger'},
                                            {text: editor.getLang('ef_shtd_lang.field_warning'), value: 'panel-warning'},
                                            {text: editor.getLang('ef_shtd_lang.field_info'), value: 'panel-info'},
                                            {text: editor.getLang('ef_shtd_lang.field_success'), value: 'panel-success'}
                                        ]
                                    },

                                    // Panel heading
                                    {
                                        type: 'textbox',
                                        name: 'panel_title',
                                        label: editor.getLang('ef_shtd_lang.field_title'),
                                        classes: 'validate[not_empty]',
                                        value: 'This is heading'
                                    },

                                    // Panel text
                                    {
                                        type: 'textbox',
                                        name: 'panel_text',
                                        multiline: true,
                                        minHeight: 140,
                                        label: editor.getLang('ef_shtd_lang.field_content'),
                                        classes: 'validate[not_empty]',
                                        value: '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>'
                                    }],

                                    onsubmit: function(e) {
                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-panel heading="' + e.data.panel_title + '" type="' + e.data.panel_type + '"]' + e.data.panel_text + '[/ef-panel]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        }, // End Panels

                        /* Alerts */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_alerts'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 160,
                                    title: editor.getLang('ef_shtd_lang.shtd_alerts'),
                                    body: [

                                    // Alert type
                                    {
                                        type: 'listbox',
                                        name: 'alert_type',
                                        label: editor.getLang('ef_shtd_lang.field_type'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.field_danger'), value: 'alert-danger'},
                                            {text: editor.getLang('ef_shtd_lang.field_warning'), value: 'alert-warning'},
                                            {text: editor.getLang('ef_shtd_lang.field_info'), value: 'alert-info'},
                                            {text: editor.getLang('ef_shtd_lang.field_success'), value: 'alert-success'}
                                        ]
                                    },

                                    // Alert close button
                                    {
                                        type: 'checkbox',
                                        name: 'alert_close',
                                        label: editor.getLang('ef_shtd_lang.shtd_alerts_close'),
                                        checked: true
                                    },

                                    // Alert text
                                    {
                                        type: 'textbox',
                                        name: 'alert_text',
                                        label: editor.getLang('ef_shtd_lang.field_text'),
                                        classes: 'validate[not_empty]',
                                        value: 'This is alert box'
                                    }],

                                    onsubmit: function(e) {
                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-alert-box type="' + e.data.alert_type + '" close="' + (e.data.alert_close ? 'yes' : 'no') + '"]' + e.data.alert_text + '[/ef-alert-box]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        } // End Alerts
                    ]
                }, // End jQuery section


                /** jQuery Start **/
                {
                text: editor.getLang('ef_shtd_lang.shtd_section3'),
                menu: [

                        /* Progressbars */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_progress'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 320,
                                    title: editor.getLang('ef_shtd_lang.shtd_progress'),
                                    body: [

                                    // Progressbars title
                                    {
                                        type: 'textbox',
                                        name: 'progress_title',
                                        label: editor.getLang('ef_shtd_lang.shtd_progress_skill'),
                                        classes: 'validate[not_empty]',
                                        value: 'UI/UX'
                                    },

                                    // Progressbars percent
                                    {
                                        type: 'textbox',
                                        name: 'progress_val',
                                        label: editor.getLang('ef_shtd_lang.shtd_progress_percent'),
                                        classes: 'validate[digit]',
                                        value: '60'
                                    },

                                    // Spacer
                                    {
                                        type: 'spacer',
                                        minHeight: 5
                                    },

                                    // Progressbars title
                                    {
                                        type: 'textbox',
                                        name: 'progress_title1',
                                        label: editor.getLang('ef_shtd_lang.shtd_progress_skill'),
                                        classes: 'validate[not_empty]',
                                        value: 'Adobe Photoshop'
                                    },

                                    // Progressbars percent
                                    {
                                        type: 'textbox',
                                        name: 'progress_val1',
                                        label: editor.getLang('ef_shtd_lang.shtd_progress_percent'),
                                        classes: 'validate[digit]',
                                        value: '70'
                                    },

                                    // Spacer
                                    {
                                        type: 'spacer',
                                        minHeight: 5
                                    },

                                    // Progressbars title
                                    {
                                        type: 'textbox',
                                        name: 'progress_title2',
                                        label: editor.getLang('ef_shtd_lang.shtd_progress_skill'),
                                        classes: 'validate[not_empty]',
                                        value: 'Sublime Text 2'
                                    },

                                    // Progressbars percent
                                    {
                                        type: 'textbox',
                                        name: 'progress_val2',
                                        label: editor.getLang('ef_shtd_lang.shtd_progress_percent'),
                                        classes: 'validate[digit]',
                                        value: '80'
                                    }],

                                    onsubmit: function(e) {
                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-progress-bar]<br />[ef-progress-pane percent="' + e.data.progress_val + '"]' + e.data.progress_title + '[/ef-progress-pane]<br />[ef-progress-pane percent="' + e.data.progress_val1 + '"]' + e.data.progress_title1 + '[/ef-progress-pane]<br />[ef-progress-pane percent="' + e.data.progress_val2 + '"]' + e.data.progress_title2 + '[/ef-progress-pane]<br />[/ef-progress-bar]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        }, // End Progressbars

                        /* Recent Posts */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_recent'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 320,
                                    title: editor.getLang('ef_shtd_lang.shtd_recent'),
                                    body: [

                                    // Recent Posts title
                                    {
                                        type: 'textbox',
                                        name: 'recent_title',
                                        label: editor.getLang('ef_shtd_lang.field_title'),
                                        classes: 'validate[not_empty]',
                                        value: 'Latest From The Blog'
                                    },

                                    // Recent Posts Featured Image
                                    {
                                        type: 'checkbox',
                                        name: 'recent_image',
                                        label: editor.getLang('ef_shtd_lang.recent_img'),
                                        checked: true
                                    },

                                    // Recent Posts blog link
                                    {
                                        type: 'listbox',
                                        name: 'recent_link',
                                        label: editor.getLang('ef_shtd_lang.recent_lnk'),
                                        'values': editor.getLang('ef_shtd_lang.blog_pages_array')
                                    },

                                    // Recent Posts link title
                                    {
                                        type: 'textbox',
                                        name: 'recent_link_text',
                                        label: editor.getLang('ef_shtd_lang.recent_link_title'),
                                        classes: 'validate[not_empty]',
                                        value: 'Go to Blog'
                                    },

                                    // Recent Posts exclude
                                    {
                                        type: 'listbox',
                                        name: 'recent_exclude',
                                        label: editor.getLang('ef_shtd_lang.recent_exclude_ids'),
                                        'values': editor.getLang('ef_shtd_lang.recent_exclude_ids_array')
                                    },

                                    // Spacer
                                    {
                                        type: 'spacer',
                                        minHeight: 5
                                    },

                                    // Recent Posts
                                    {
                                        type: 'textbox',
                                        name: 'recent_posts',
                                        label: editor.getLang('ef_shtd_lang.recent_num'),
                                        classes: 'validate[digit]',
                                        value: '4'
                                    },

                                    // Recent Posts per one slide
                                    {
                                        type: 'textbox',
                                        name: 'recent_per_slide',
                                        label: editor.getLang('ef_shtd_lang.recent_per'),
                                        classes: 'validate[digit]',
                                        value: '1'
                                    },

                                    // Autoplay
                                    {
                                        type: 'checkbox',
                                        name: 'recent_autoplay',
                                        label: editor.getLang('ef_shtd_lang.recent_auto'),
                                        checked: false
                                    },

                                    // Infinite
                                    {
                                        type: 'checkbox',
                                        name: 'recent_infinite',
                                        label: editor.getLang('ef_shtd_lang.recent_inf'),
                                        checked: true
                                    },

                                    // Duration
                                    {
                                        type: 'textbox',
                                        name: 'recent_duration',
                                        label: editor.getLang('ef_shtd_lang.recent_dur'),
                                        classes: 'validate[digit]',
                                        value: '2000'
                                    },

                                    // Transition
                                    {
                                        type: 'listbox',
                                        name: 'recent_transition',
                                        label: editor.getLang('ef_shtd_lang.recent_trans'),
                                        'values': [
                                            {text: 'Slide', value: 'slide'},
                                            {text: 'Crossfade', value: 'crossfade'},
                                            {text: 'Cover', value: 'cover'},
                                            {text: 'Cover-fade', value: 'cover-fade'},
                                            {text: 'Uncover', value: 'uncover'},
                                            {text: 'Uncover-fade', value: 'uncover-fade'},
                                        ]
                                    }],

                                    onsubmit: function(e) {
                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-recent-posts number="' + e.data.recent_posts + '" image="' + (e.data.recent_image ? 'yes' : 'no') + '" in_slide="' + e.data.recent_per_slide + '" exclude="' + e.data.recent_exclude + '" link="' + e.data.recent_link + '" link_title="' + e.data.recent_link_text + '" autoplay="' + (e.data.recent_autoplay ? 'yes' : 'no') + '" infinite="' + (e.data.recent_infinite ? 'yes' : 'no') + '" duration="' + e.data.recent_duration + '" fx="' + e.data.recent_transition + '"]' + e.data.recent_title + '[/ef-recent-posts]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        }, // End Recent Posts

                        /* Slideshow */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_slider'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 320,
                                    title: editor.getLang('ef_shtd_lang.shtd_slider'),
                                    body: [

                                    // Slideshow title
                                    {
                                        type: 'textbox',
                                        name: 'sld_title',
                                        label: editor.getLang('ef_shtd_lang.field_title'),
                                        classes: 'validate[not_empty]',
                                        value: 'Title goes here'
                                    },

                                    // Slideshow type
                                    {
                                        type: 'listbox',
                                        name: 'sld_type',
                                        label: editor.getLang('ef_shtd_lang.field_type'),
                                        'values': [
                                            {text: editor.getLang('ef_shtd_lang.shtd_slider'), value: 'slider'},
                                            {text: editor.getLang('ef_shtd_lang.slideshow_type1'), value: 'carousel'}
                                        ]
                                    },

                                    // Slideshow slides
                                    {
                                        type: 'textbox',
                                        name: 'sld_slides',
                                        label: editor.getLang('ef_shtd_lang.slideshow_slides'),
                                        classes: 'validate[digit]',
                                        value: '3'
                                    },

                                    // Autoplay
                                    {
                                        type: 'checkbox',
                                        name: 'sld_autoplay',
                                        label: editor.getLang('ef_shtd_lang.recent_auto'),
                                        checked: false
                                    },

                                    // Infinite
                                    {
                                        type: 'checkbox',
                                        name: 'sld_infinite',
                                        label: editor.getLang('ef_shtd_lang.recent_inf'),
                                        checked: true
                                    },

                                    // Duration
                                    {
                                        type: 'textbox',
                                        name: 'sld_duration',
                                        label: editor.getLang('ef_shtd_lang.recent_dur'),
                                        classes: 'validate[digit]',
                                        value: '2000'
                                    },

                                    // Transition
                                    {
                                        type: 'listbox',
                                        name: 'sld_transition',
                                        label: editor.getLang('ef_shtd_lang.recent_trans'),
                                        'values': [
                                            {text: 'Slide', value: 'slide'},
                                            {text: 'Crossfade', value: 'crossfade'},
                                            {text: 'Cover', value: 'cover'},
                                            {text: 'Cover-fade', value: 'cover-fade'},
                                            {text: 'Uncover', value: 'uncover'},
                                            {text: 'Uncover-fade', value: 'uncover-fade'},
                                        ]
                                    }],

                                    onsubmit: function(e) {
                                        var oldslide,
                                            textImg = e.data.sld_type == 'carousel' ? '<p>Any content goes here</p>' : '<img style="display: block" src="' + editor.getLang('ef_shtd_lang.slideshow_img') + 'img/placeholder.jpg" alt="" />',
                                            slide = '[ef-slide]' + textImg + '[/ef-slide]<br />';

                                        for (var i = 1; i < e.data.sld_slides; i++) {
                                            oldslide = slide;
                                            slide = '[ef-slide]' + textImg + '[/ef-slide]<br />';
                                            slide = oldslide + slide;
                                        }

                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields() && e.data.sld_slides > 0){
                                            editor.insertContent('[ef-slider title="' + e.data.sld_title + '" type="' + e.data.sld_type + '" autoplay="' + (e.data.sld_autoplay ? 'yes' : 'no') + '" infinite="' + (e.data.sld_infinite ? 'yes' : 'no') + '" duration="' + e.data.sld_duration + '" fx="' + e.data.sld_transition + '"]<br />' + slide + '[/ef-slider]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        },

                        /* jQuery UI */
                        {
                            text: editor.getLang('ef_shtd_lang.shtd_ui'),
                            onclick: function() {
                                editor.windowManager.open({
                                    classes: 'ef-shtd-body',
                                    width: 450,
                                    height: 320,
                                    title: editor.getLang('ef_shtd_lang.shtd_ui'),
                                    body: [

                                    // UI type
                                    {
                                        type: 'listbox',
                                        name: 'ui_type',
                                        label: editor.getLang('ef_shtd_lang.field_type'),
                                        'values': [
                                            {text: 'Accordion', value: 'accordion'},
                                            {text: 'Toggle boxes', value: 'toggles'},
                                            {text: 'Tabs', value: 'tabs'}
                                        ]
                                    },

                                    // jQuery UI title
                                    {
                                        type: 'textbox',
                                        name: 'ui_title1',
                                        label: editor.getLang('ef_shtd_lang.field_title'),
                                        classes: 'validate[not_empty]',
                                        value: 'Heading'
                                    },

                                    // jQuery UI content
                                    {
                                        type: 'textbox',
                                        name: 'ui_content1',
                                        multiline: true,
                                        minHeight: 100,
                                        label: editor.getLang('ef_shtd_lang.field_content'),
                                        classes: 'validate[not_empty]',
                                        value: '<p>Any content goes here<p>'
                                    },

                                    // Spacer
                                    {
                                        type: 'spacer',
                                        minHeight: 5
                                    },

                                    // jQuery UI title
                                    {
                                        type: 'textbox',
                                        name: 'ui_title2',
                                        label: editor.getLang('ef_shtd_lang.field_title'),
                                        classes: 'validate[not_empty]',
                                        value: 'Heading'
                                    },

                                    // jQuery UI content
                                    {
                                        type: 'textbox',
                                        name: 'ui_content2',
                                        multiline: true,
                                        minHeight: 100,
                                        label: editor.getLang('ef_shtd_lang.field_content'),
                                        classes: 'validate[not_empty]',
                                        value: '<p>Any content goes here<p>'
                                    }],

                                    onsubmit: function(e) {
                                        var shtd;
                                        if (e.data.ui_type == 'tabs') {
                                            shtd = '[ef-ui-tabs-nav]<br />[ef-ui-header id="#ef-tab-1" for="' + e.data.ui_type + '"]' + e.data.ui_title1 + '[/ef-ui-header]<br />[ef-ui-header id="#ef-tab-2" for="' + e.data.ui_type + '"]' + e.data.ui_title2 + '[/ef-ui-header]<br />[/ef-ui-tabs-nav]<br />[ef-ui-pane id="ef-tab-1" for="' + e.data.ui_type + '"]' + e.data.ui_content1 + '[/ef-ui-pane]<br />[ef-ui-pane id="ef-tab-2" for="' + e.data.ui_type + '"]' + e.data.ui_content2 + '[/ef-ui-pane]<br />';
                                        } else {
                                            shtd = '[ef-ui-header for="' + e.data.ui_type + '"]' + e.data.ui_title1 + '[/ef-ui-header]<br />[ef-ui-pane for="' + e.data.ui_type + '"]' + e.data.ui_content1 + '[/ef-ui-pane]<br />[ef-ui-header for="' + e.data.ui_type + '"]' + e.data.ui_title2 + '[/ef-ui-header]<br />[ef-ui-pane for="' + e.data.ui_type + '"]' + e.data.ui_content2 + '[/ef-ui-pane]<br />';
                                        }

                                        if (jQuery('.mce-ef-shtd-body').ef_validate_fields()){
                                            editor.insertContent('[ef-ui-element type="' + e.data.ui_type + '"]<br />' + shtd + '[/ef-ui-element]');
                                        } else {
                                            return false;
                                        }
                                    }
                                });
                            }
                        } // End jQuery UI
                    ]
                } // End jQuery section

            ]
        });

        // function ef_shtd_replaceshortcodes(content) {
        //     return content.replace( /\[ef-*([^\]]*)\]([^\]]*)\[\/ef-.*]/g, function( match ) {
        //         return ef_shtd_html( match, content );
        //     });
        // }

        // function ef_shtd_html(data, content) {
        //     data = window.encodeURIComponent(data);
        //     content = window.encodeURIComponent(content);

        //     return '<img class="ef-shtd-mceitem mceItem" src="' + tinymce.Env.transparentSrc + '" data-mce-content="' + content + '" data-mce-resize="false" data-mce-placeholder="1" />';
        // }

        // function ef_decode_attr(s, n) {
        //     n = new RegExp(n + '=\"([^\"]+)\"', 'g').exec(s);
        //     return n ? window.decodeURIComponent(n[1]) : '';
        // }

        // function ef_shtd_restoreshortcodes(content) {
        //     return content.replace( /(?:<p(?: [^>]+)?>)*(<img [^>]+>)(?:<\/p>)*/g, function(match, shtd) {
        //         var data = ef_decode_attr(shtd, 'data-mce-content');

        //         if (data) {
        //             return data;
        //         }
        //         return match;
        //     });
        // }

        // editor.on('GetContent', function(event){
        //     event.content = ef_shtd_restoreshortcodes(event.content);
        // });

        // editor.on('BeforeSetContent', function(event) {
        //     event.content = ef_shtd_replaceshortcodes(event.content);
        // });
    });
})();