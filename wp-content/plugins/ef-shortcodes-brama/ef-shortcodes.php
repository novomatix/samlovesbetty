<?php

/*
Plugin Name: Fireform Shortcodes - Brama
Plugin URI: http://themeforest.net/users/fireform
Description: This is a required plugin for "Brama" theme. This Plugin adds tiny buttons to TinyMCE Editor for creating shortcodes without the need to code.
Author: Evgeny Fireform
Author URI: http://themeforest.net/users/fireform
Version: 1.2
License: Regular License
License URI: http://themeforest.net/licenses
*/

/*  Copyright 2014  Evgeny Fireform. Feel free to contact me via my profile page contact form.

    The Regular License allows use of the item in one single end product which end users are not charged to access or use. You can do this directly or, if you're a freelancer, you can create the end product for one client to distribute free to its end users. You can charge your client to produce the single end product. Distribution of source files is not permitted.
*/

defined( 'ABSPATH' ) || exit;

/**
 * Add theme support
 *
 * @since 1.0
 */
add_theme_support( 'ef-shortcodes' );


/**
 * Definitions
 *
 * @since 1.0
 */
define( 'SHTD_VERSION', '1.2' );

if ( !defined( 'SHTD_URL' ) ) {
	define( 'SHTD_URL', plugin_dir_url( __FILE__ ) );
	define( 'SHTD_ADM_URL', trailingslashit( SHTD_URL . 'shortcodes' ) );
	define( 'SHTD_JS_URL', trailingslashit( SHTD_ADM_URL . 'scripts' ) );
}

if ( !defined( 'SHTD_DIR' ) ) {
	define( 'SHTD_DIR', plugin_dir_path( __FILE__ ) );
	define( 'SHTD_ADM_DIR', trailingslashit( SHTD_DIR . 'shortcodes' ) );
}

if ( !defined( 'EF_SHTD' ) ) {
	define( 'EF_SHTD', 'ef-shortcodes' );
}

/**
 * Required Files
 *
 * @since 1.0
 */
require_once SHTD_ADM_DIR . 'shortcodes.php';

if ( is_admin() ) {
	require_once SHTD_ADM_DIR . 'buttons.php';
}

require_once SHTD_ADM_DIR . 'functions.php';
