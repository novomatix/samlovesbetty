<?php

/**
 * Register CPTs
 *
 * @since 1.0
 */

function ef_register_cpt() {

// Register portfolio custom post type
	$labels = array(
		'name'               => _x( 'Posts', 'post type general name', 'ef-cpt' ),
		'singular_name'      => _x( 'Portfolio', 'post type singular name', 'ef-cpt' ),
		'add_new'            => _x( 'Add New', 'Portfolio post', 'ef-cpt' ),
		'add_new_item'       => __( 'Add New Portfolio post', 'ef-cpt' ),
		'edit_item'          => __( 'Edit Portfolio post', 'ef-cpt' ),
		'new_item'           => __( 'New Portfolio post', 'ef-cpt' ),
		'all_items'          => __( 'All Posts', 'ef-cpt' ),
		'view_item'          => __( 'View Portfolio post', 'ef-cpt' ),
		'search_items'       => __( 'Search Portfolio posts', 'ef-cpt' ),
		'not_found'          => __( 'No Portfolio posts found', 'ef-cpt' ),
		'not_found_in_trash' => __( 'No Portfolio posts found in the Trash', 'ef-cpt' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Portfolio', 'ef-cpt' ),
	);

	$args = array(
		'labels'        => $labels,
		'description'   => __( 'Holds Portfolio specific data', 'ef-cpt' ),
		'public'        => true,
		'menu_position' => 5,
		'rewrite' 		=> array( 'slug' => EF_CPT_SLUG, 'with_front' => false ),
		'query_var' 	=> true,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => true,
		'taxonomies' 	=> array( EF_CPT_TAX_CAT, EF_CPT_TAX_TAG ),
		'menu_icon'		=> 'dashicons-format-gallery',
		'exclude_from_search'	=> false,
	);

	register_post_type( EF_CPT, $args );

// Register portfolio categories
	$labels = array(
		'name'              => _x( 'Portfolio categories', 'taxonomy general name', 'ef-cpt' ),
		'singular_name'     => _x( 'Portfolio category', 'taxonomy singular name', 'ef-cpt' ),
		'search_items'      => __( 'Search Portfolio categories', 'ef-cpt' ),
		'all_items'         => __( 'All Portfolio categories', 'ef-cpt' ),
		'parent_item'       => __( 'Parent Portfolio category', 'ef-cpt' ),
		'parent_item_colon' => __( 'Parent Portfolio category:', 'ef-cpt' ),
		'edit_item'         => __( 'Edit Portfolio category', 'ef-cpt' ),
		'update_item'       => __( 'Update Portfolio category', 'ef-cpt' ),
		'add_new_item'      => __( 'Add New Portfolio category', 'ef-cpt' ),
		'new_item_name'     => __( 'New Portfolio Category', 'ef-cpt' ),
		'menu_name'         => __( 'Categories', 'ef-cpt' ),
	);

	$args_tax = array(
		'labels'		=> $labels,
		'show_ui'    	=> true,
		'show_admin_column' => true,
		'show_in_nav_menus' => true,
		'hierarchical'	=> true,
		'capability_type'	=> 'post',
		'rewrite'		=> array( 'slug' => EF_CPT_TAX_CAT, 'with_front' => false ),
		'exclude_from_search'	=> false,
	);

	register_taxonomy( EF_CPT_TAX_CAT, EF_CPT, $args_tax );

// Register portfolio tags
	$labels = array(
		'name'              => _x( 'Portfolio tags', 'taxonomy general name', 'ef-cpt' ),
		'singular_name'     => _x( 'Portfolio tag', 'taxonomy singular name', 'ef-cpt' ),
		'search_items'      => __( 'Search Portfolio tags', 'ef-cpt' ),
		'all_items'         => __( 'All Portfolio tags', 'ef-cpt' ),
		'parent_item'       => __( 'Parent Portfolio tag', 'ef-cpt' ),
		'parent_item_colon' => __( 'Parent Portfolio tag:', 'ef-cpt' ),
		'edit_item'         => __( 'Edit Portfolio tag', 'ef-cpt' ),
		'update_item'       => __( 'Update Portfolio tag', 'ef-cpt' ),
		'add_new_item'      => __( 'Add New Portfolio tag', 'ef-cpt' ),
		'new_item_name'     => __( 'New Portfolio tag', 'ef-cpt' ),
		'menu_name'         => __( 'Tags', 'ef-cpt' ),
	);

	$args_tags = array(
		'labels' => $labels,
		'hierarchical' => false,
		'rewrite' => array( 'slug' => EF_CPT_TAX_TAG, 'with_front' => false ),
		'exclude_from_search'	=> false,
	);

	register_taxonomy( EF_CPT_TAX_TAG, EF_CPT, $args_tags );

// Register Extras
	$labels = array(
		'name'               => _x( 'Posts', 'post type general name', 'ef-cpt' ),
		'singular_name'      => _x( 'Extra', 'post type singular name', 'ef-cpt' ),
		'add_new'            => _x( 'Add New', 'Extra post', 'ef-cpt' ),
		'add_new_item'       => __( 'Add New Extra post', 'ef-cpt' ),
		'edit_item'          => __( 'Edit Extra post', 'ef-cpt' ),
		'new_item'           => __( 'New Extra post', 'ef-cpt' ),
		'all_items'          => __( 'All Posts', 'ef-cpt' ),
		'view_item'          => __( 'View Extra post', 'ef-cpt' ),
		'search_items'       => __( 'Search Extra posts', 'ef-cpt' ),
		'not_found'          => __( 'No Extra posts found', 'ef-cpt' ),
		'not_found_in_trash' => __( 'No Extra posts found in the Trash', 'ef-cpt' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Extras', 'ef-cpt' ),
	);

	$args = array(
		'labels'        => $labels,
		'description'   => __( 'Holds Extras specific data', 'ef-cpt' ),
		'public'        => true,
		'menu_position' => 6,
		'rewrite' 		=> array( 'slug' => EF_CPT_EXTR_SLUG, 'with_front' => false ),
		'query_var' 	=> true,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => false,
		'taxonomies' 	=> array( EF_CPT_EXTR_TAX ),
		'menu_icon'		=> 'dashicons-megaphone'
	);

	register_post_type( EF_CPT_EXTR, $args );

// Register Extra categories
	$labels = array(
		'name'              => _x( 'Extra categories', 'taxonomy general name', 'ef-cpt' ),
		'singular_name'     => _x( 'Extra category', 'taxonomy singular name', 'ef-cpt' ),
		'search_items'      => __( 'Search Extra categories', 'ef-cpt' ),
		'all_items'         => __( 'All Extra categories', 'ef-cpt' ),
		'parent_item'       => __( 'Parent Extra category', 'ef-cpt' ),
		'parent_item_colon' => __( 'Parent Extra category:', 'ef-cpt' ),
		'edit_item'         => __( 'Edit Extra category', 'ef-cpt' ),
		'update_item'       => __( 'Update Extra category', 'ef-cpt' ),
		'add_new_item'      => __( 'Add New Extra category', 'ef-cpt' ),
		'new_item_name'     => __( 'New Extra category', 'ef-cpt' ),
		'menu_name'         => __( 'Categories', 'ef-cpt' ),
	);

	$args_tax = array(
		'labels'			=> $labels,
		'show_ui'			=> true,
		'show_admin_column' => true,
		'show_in_nav_menus' => false,
		'hierarchical' => true,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => EF_CPT_EXTR_TAX, 'with_front' => false ),
		'exclude_from_search'	=> true,
	);

	register_taxonomy( EF_CPT_EXTR_TAX, EF_CPT_EXTR, $args_tax );

	// Register Team
	$labels = array(
		'name'               => _x( 'Members', 'post type general name', 'ef-cpt' ),
		'singular_name'      => _x( 'Member', 'post type singular name', 'ef-cpt' ),
		'add_new'            => _x( 'Add New', 'Member', 'ef-cpt' ),
		'add_new_item'       => __( 'Add New Member', 'ef-cpt' ),
		'edit_item'          => __( 'Edit Member', 'ef-cpt' ),
		'new_item'           => __( 'New Member', 'ef-cpt' ),
		'all_items'          => __( 'All Membes', 'ef-cpt' ),
		'view_item'          => __( 'View Member', 'ef-cpt' ),
		'search_items'       => __( 'Search Members', 'ef-cpt' ),
		'not_found'          => __( 'No Members found', 'ef-cpt' ),
		'not_found_in_trash' => __( 'No Members found in the Trash', 'ef-cpt' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Team', 'ef-cpt' ),
	);

	$args = array(
		'labels'        => $labels,
		'description'   => __( 'Holds Member specific data', 'ef-cpt' ),
		'public'        => true,
		'menu_position' => 6,
		'rewrite' 		=> array( 'slug' => EF_CPT_TEAM_SLUG, 'with_front' => false ),
		'query_var' 	=> true,
		'supports'      => array( 'title', 'editor', 'thumbnail' ),
		'has_archive'   => false,
		'taxonomies' 	=> array( EF_CPT_TEAM_TAX ),
		'exclude_from_search'	=> true,
		'menu_icon'		=> 'dashicons-groups'
	);

	register_post_type( EF_CPT_TEAM, $args );

// Register Team categories
	$labels = array(
		'name'              => _x( 'Team categories', 'taxonomy general name', 'ef-cpt' ),
		'singular_name'     => _x( 'Team category', 'taxonomy singular name', 'ef-cpt' ),
		'search_items'      => __( 'Search Team categories', 'ef-cpt' ),
		'all_items'         => __( 'All Team categories', 'ef-cpt' ),
		'parent_item'       => __( 'Parent Team category', 'ef-cpt' ),
		'parent_item_colon' => __( 'Parent Team category:', 'ef-cpt' ),
		'edit_item'         => __( 'Edit Team category', 'ef-cpt' ),
		'update_item'       => __( 'Update Team category', 'ef-cpt' ),
		'add_new_item'      => __( 'Add New Team category', 'ef-cpt' ),
		'new_item_name'     => __( 'New Team category', 'ef-cpt' ),
		'menu_name'         => __( 'Categories', 'ef-cpt' ),
	);

	$args_tax = array(
		'labels'			=> $labels,
		'show_ui'			=> true,
		'show_admin_column' => true,
		'show_in_nav_menus' => false,
		'hierarchical' => true,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => EF_CPT_TEAM_TAX, 'with_front' => false ),
		'exclude_from_search'	=> true,
	);

	register_taxonomy( EF_CPT_TEAM_TAX, EF_CPT_TEAM, $args_tax );

	// Register custom widget wreas
	$labels = array(
		'name'               => _x( 'Widget areas', 'post type general name', 'ef-cpt' ),
		'singular_name'      => _x( 'Widget area', 'post type singular name', 'ef-cpt' ),
		'add_new'            => _x( 'Create New', 'Widget area', 'ef-cpt' ),
		'add_new_item'       => __( 'Create New Widget area', 'ef-cpt' ),
		'edit_item'          => __( 'Edit widget area', 'ef-cpt' ),
		'new_item'           => __( 'New widget area', 'ef-cpt' ),
		'all_items'          => __( 'All widget areas', 'ef-cpt' ),
		'view_item'          => __( 'View widget area', 'ef-cpt' ),
		'search_items'       => __( 'Search widget areas', 'ef-cpt' ),
		'not_found'          => __( 'No widget areas found', 'ef-cpt' ),
		'not_found_in_trash' => __( 'No widget areas found in the Trash', 'ef-cpt' ),
		'parent_item_colon'  => '',
		'menu_name'          => __( 'Widget areas', 'ef-cpt' ),
	);

	$args = array(
		'labels'        => $labels,
		'description'   => __( 'Holds Widget areas specific data', 'ef-cpt' ),
		'public'        => true,
		'menu_position' => 7,
		'rewrite' 		=> false,
		'exclude_from_search'	=> true,
		'query_var' 	=> false,
        'show_in_nav_menus' => false,
		'supports'      => array( 'title' ),
		'has_archive'   => false,
		'menu_icon'		=> 'dashicons-feedback',
	);

	register_post_type( EF_CPT_SIDEBARS, $args );


}

add_action( 'init', 'ef_register_cpt' );


/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'ef_flush_rewrite_rules_cpt' );
function ef_flush_rewrite_rules_cpt() {
	flush_rewrite_rules();
}