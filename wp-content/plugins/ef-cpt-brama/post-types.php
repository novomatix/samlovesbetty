<?php

/*
Plugin Name: Fireform CPT - Brama
Plugin URI: http://themeforest.net/users/fireform
Description: This is a required plugin for "Brama" theme. This plugin adds new custom post types: "Extras" and "Portfolios".
Author: Evgeny Fireform
Author URI: http://themeforest.net/users/fireform
Version: 1.0
License: ThemeForest Regular License
License URI: http://themeforest.net/licenses
*/

/*  Copyright 2014  Evgeny Fireform. Feel free to contact me via my profile page contact form.

    The Regular License allows use of the item in one single end product which end users are not charged to access or use. You can do this directly or, if you're a freelancer, you can create the end product for one client to distribute free to its end users. You can charge your client to produce the single end product. Distribution of source files is not permitted.
*/


defined( 'ABSPATH' ) || exit;


/**
 * Definitions
 *
 * @since 1.0
 */
define( 'EF_CPT_VERSION', '1.0' );


/**
 * Add theme support
 *
 * @since 1.0
 */
add_theme_support( 'ef-custom-post-types' );


/**
 * CPT constants
 *
 * @since 1.0
 */
if( !defined( 'EF_CPT' ) ) {
	define( 'EF_CPT', 'portfolios' );
	define( 'EF_CPT_SLUG', 'projects' );
	define( 'EF_CPT_TAX_CAT', 'portfolio-category' );
	define( 'EF_CPT_TAX_TAG', 'portfolio-tag' );

	define( 'EF_CPT_EXTR', 'extras' );
	define( 'EF_CPT_EXTR_SLUG', 'extra' );
	define( 'EF_CPT_EXTR_TAX', 'extra-category' );

	define( 'EF_CPT_TEAM', 'team' );
	define( 'EF_CPT_TEAM_SLUG', 'member' );
	define( 'EF_CPT_TEAM_TAX', 'team-category' );

	define( 'EF_CPT_SIDEBARS', 'custom-sidebars' );

	define( 'EF_CPT_URL', plugin_dir_url( __FILE__ ) );
}

if( !defined( 'EF_CPT_DIR' ) ) {
	define( 'EF_CPT_DIR', plugin_dir_path( __FILE__ ) );
	define( 'EF_CPT_ADM_DIR', trailingslashit( EF_CPT_DIR . 'admin' ) );
}

/**
 * Required Files
 *
 * @since 1.0
 */
require_once EF_CPT_ADM_DIR . 'register.php';
require_once EF_CPT_ADM_DIR . 'functions.php';