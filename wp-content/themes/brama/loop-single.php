<?php
/**
 * The loop that displays a single post.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

	<?php if ( have_posts() ) { ?>

		<?php while ( have_posts() ) : the_post(); ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<header class="entry-header text-center">
							<?php $icon_extra = $ef_data['mb_support'] ? efmb_meta( 'ef_extra_icons', 'type=layout' ) : null; ?>

							<?php if ( !empty( $icon_extra ) ) { ?>
								<span class="ef-service-icn <?php echo $icon_extra ?>"></span>
							<?php } elseif ( !( $ef_data['cpt_support'] && ( $post->post_type == EF_CPT_EXTR || $post->post_type == EF_CPT_TEAM ) ) ) { ?>
								<?php echo ef_theme_posted_on( $post, $ef_data ); ?>
							<?php } ?>

			 				<h1 class="entry-title ef-page-title ef-underlined-title text-center">
			 					<?php the_title(); ?>
			 				</h1>
		 				</header>
					</div>
				</div>

				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="entry-content">
							<?php the_content(); ?>

							<?php /* Page links */ ?>
							<?php wp_link_pages( array(
								'before'		=> '<div class="ef-nextpage"><span class="ef-page-links-title">' . __( 'Pages:', EF_TDM ) . '</span>',
								'after'	=> '</div>',
								'before_link'	=> '<span>',
								'after_link'	=> '</span>'
							) ); ?>
						</div>

						<?php if ( count( $ef_data['project_details'] ) > 0 ) { ?>
							<div id="ef-project-details" class="row">
								<div class="col-md-12">
									<?php if ( !empty( $ef_data['project_details']['client'] ) ) { ?>
										<div class="row">
											<div class="col-md-3">
												<header><?php _e( 'Client', EF_TDM ); ?></header>
											</div>

											<div class="col-md-9">
												<p>
													<?php echo wp_kses( $ef_data['project_details']['client'], array() ); ?>
												</p>
											</div>
										</div>
									<?php } ?>

									<?php if ( !empty( $ef_data['project_details']['services'] ) ) { ?>
										<div class="row">
											<div class="col-md-3">
												<header><?php _e( 'Services', EF_TDM ); ?></header>
											</div>

											<div class="col-md-9">
												<p>
													<?php echo wp_kses( $ef_data['project_details']['services'], array() ); ?>
												</p>
											</div>
										</div>
									<?php } ?>

									<?php if ( !empty( $ef_data['project_details']['website'] ) ) { ?>
										<div class="row">
											<div class="col-md-3">
												<header><?php _e( 'Website', EF_TDM ); ?></header>
											</div>

											<div class="col-md-9">
												<p>
													<a href="<?php echo esc_url( $ef_data['project_details']['website'] ); ?>"><?php echo esc_url( $ef_data['project_details']['website'] ); ?></a>
												</p>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						<?php } ?>
					</div>
				</div>

				<?php $terms = $ef_data['cpt_support'] && is_singular( EF_CPT ) ? get_the_terms( $post->ID, EF_CPT_TAX_TAG ) : ''; ?>

				<?php if ( !empty( $terms ) ) { ?>
					<div class="row">
						<div class="col-md-8 col-md-offset-2">
							<div class="row">
								<div class="col-md-3">
									<strong class="ef-share-title"><?php _e( 'Tags', EF_TDM ); ?></strong>
								</div>

								<div class="col-md-9">
									<?php echo ef_theme_posted_in( $post, $ef_data ); ?>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>

				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<?php if ( !post_password_required() && !( $ef_data['cpt_support'] && ( $post->post_type == EF_CPT_EXTR || $post->post_type == EF_CPT_TEAM ) ) ) { ?>

							<div id="ef-entry-meta" class="row">
								<div class="<?php echo $post->post_type == 'post' ? 'col-sm-7 text-center-xs' : 'col-md-3'; ?>">
									<?php if ( $post->post_type == 'post' ) { ?>
										<?php echo ef_theme_posted_in( $post, $ef_data ); ?>
									<?php } elseif ( ef_add_share_btns( $ef_data, get_the_title(),  get_permalink() ) ) { ?>
										<strong class="ef-share-title"><?php _e( 'Share', EF_TDM ); ?></strong>
									<?php } ?>
								</div>

								<div class="<?php echo $post->post_type == 'post' ? 'col-sm-5 text-right-md text-right-sm text-right-lg text-center-xs' : 'col-md-9'; ?>">
									<?php if ( ef_add_share_btns( $ef_data, get_the_title(), get_permalink() ) ) { ?>
										<?php if ( $post->post_type == 'post' ) { ?>
											<?php _e( 'Share by', EF_TDM ); ?>
										<?php } ?>
										<?php echo ef_add_share_btns( $ef_data, esc_attr( get_the_title() ), esc_url( get_permalink() ) ); ?>
									<?php } ?>
								</div>
							</div>

							<?php echo ef_theme_author_info(); ?>

							<hr>
						<?php } ?>

						<?php if ( $ef_data['to_support'] && ( ( $ef_data['cpt_support'] && is_singular( EF_CPT ) && $ef_data['ef_post_nav']['1'] ) || ( is_singular( 'post' ) && $ef_data['ef_post_nav']['2'] ) || ( $ef_data['cpt_support'] && is_singular( EF_CPT_EXTR ) && $ef_data['ef_post_nav']['3'] ) ) ) { ?>
							<?php ef_theme_post_nav(); ?>
						<?php } ?>

						<?php edit_post_link( __( 'Edit post', EF_TDM ), '<div class="ef-post-edit-link icon-ef-pencil-1">', '</div>' ); ?>
					</div><!-- .col-md-8.col-md-offset-2 -->
				</div><!-- .row -->
			</div><!-- #post-## -->

			<?php if ( !post_password_required() && ( ( comments_open() || get_comments_number() ) && !post_password_required() && ( $ef_data['cpt_support'] && $ef_data['to_support'] && is_singular( EF_CPT ) && $ef_data['ef_portf_comments'] ) || is_singular( 'post' ) ) ) { ?>
				<?php comments_template( '', true ); ?>
			<?php } ?>
		<?php endwhile; // end of the loop. ?>
	<?php } ?>