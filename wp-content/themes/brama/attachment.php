<?php
/**
 * The template for displaying attachments.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

		<?php while ( have_posts() ) { ?>

			<?php the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?>>

				<?php $attachments = array_values(
						get_children( array(
							'post_parent'	=> $post->post_parent,
							'post_status'	=> 'inherit',
							'post_type'		=> 'attachment',
							'post_mime_type'=> 'image',
							'order' 		=> 'ASC',
							'orderby'		=> 'menu_order ID'
						)
					)
				); ?>

				<?php foreach ( $attachments as $n => $attachment ) { ?>
					<?php if ( $attachment->ID == $post->ID ) { ?>
						<?php break; ?>
					<?php } ?>
				<?php } ?>

				<?php $n++; ?>
				<?php // If there is more than 1 attachment in a gallery ?>
				<?php if ( count( $attachments ) > 1 ) { ?>
					<?php if ( isset( $attachments[ $n ] ) ) { ?>
						<?php $next_attachment_url = get_attachment_link( $attachments[ $n ]->ID ); ?>
					<?php } else { ?>
						<?php $next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID ); ?>
					<?php } ?>
				<?php } else { ?>
					<?php $next_attachment_url = wp_get_attachment_url(); ?>
				<?php } ?>

				<div class="ef-attachment-img text-center">
					<a href="<?php echo esc_url( $next_attachment_url ); ?>" title="<?php the_title_attribute(); ?>">
						<?php echo wp_get_attachment_image( $post->ID, 'large_thumb' ); ?>
					</a>

					<div class="ef-edit-attachment-link">
						<?php edit_post_link( '' ); ?>

						<?php if ( ! empty( $post->post_excerpt ) ) { ?>
							<span class="ef-entry-caption">
								<?php the_excerpt(); ?>
							</span>
						<?php } ?>
					</div>
				</div><!-- .ef-attachment-img -->

				<div class="ef-post-info text-center">
					<?php echo ef_theme_posted_on( $post, $ef_data ); ?>
				</div>

				<?php the_content(); ?>

				<ul class="ef-attachment-nav list-inline text-center">
					<li class="ef-prev-attachment"><?php previous_image_link( array( 80, 80 ) ); ?></li>

					<li class="ef-next-attachment"><?php next_image_link( array( 80, 80 ) ); ?></li>
				</ul><!-- .ef-image-navigation -->

				<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', EF_TDM ), 'after' => '</div>' ) ); ?>
			</article><!-- #post-## -->
			<?php comments_template(); ?>
		<?php } // end of the loop. ?>

<?php get_footer(); ?>
