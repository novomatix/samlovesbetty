<?php

	if ( !isset( $options ) ) {
		header( "Content-type: text/css; charset: UTF-8" );

		$options = $GLOBALS['efto_data'];
	}

	/**

	  Color adjustifier.
	  @since Brama 1.0

	* */
	function ef_adjustBrightness( $hex, $steps ) {
		$steps = max( -255, min( 255, $steps ) );

		$hex = str_replace( '#', '', $hex );
		if ( strlen( $hex ) == 3 ) {
			$hex = str_repeat( substr( $hex,0,1 ), 2 ).str_repeat( substr( $hex,1,1 ), 2 ).str_repeat( substr( $hex,2,1 ), 2 );
		}

		$r = hexdec( substr( $hex,0,2 ) );
		$g = hexdec( substr( $hex,2,2 ) );
		$b = hexdec( substr( $hex,4,2 ) );

		$r = max( 0,min( 255,$r + $steps ) );
		$g = max( 0,min( 255,$g + $steps ) );
		$b = max( 0,min( 255,$b + $steps ) );

		$r_hex = str_pad( dechex( $r ), 2, '0', STR_PAD_LEFT );
		$g_hex = str_pad( dechex( $g ), 2, '0', STR_PAD_LEFT );
		$b_hex = str_pad( dechex( $b ), 2, '0', STR_PAD_LEFT );

		return array(
			'color' => '#'.$r_hex.$g_hex.$b_hex,
			'brightness' => ( ( $r * 299 ) + ( $g * 587 ) + ( $b * 114 ) ) / 1000
		);
	}


	$main_col = $options['ef_theme_color'];
	$dark_col = ef_adjustBrightness( $main_col, -50 );
	$dark_col = $dark_col['color'];
	$light_col = ef_adjustBrightness( $main_col, 20 );
	$light_col = $light_col['color'];
	$is_dark = ef_adjustBrightness( $main_col, 0 );
	$is_dark = $is_dark['brightness'] < 130;
?>

<?php if ( !empty( $options['ef_base_offset']['width'] ) ) { ?>
.ef-home-default .slide_desc, .ef-home-default .slide_title, html.no-js .ef-home-default .html-desc, html.no-js .ef-home-default .html-title {
    left: <?php echo $options['ef_base_offset']['width']; ?>;
}
#ef-header-inner, #ef-content, #ef-site-nav-inner, #ef-main-nav-inner  {
    padding-left: <?php echo $options['ef_base_offset']['width']; ?>;
    padding-right: <?php echo $options['ef_base_offset']['width']; ?>;
}
<?php } ?>

a,
.text-primary,
.btn-link,
a.list-group-item.active > .badge,
.nav-pills > .active > a > .badge,
.pagination > .active > a,
.pagination > .active > a:hover,
.form-submit:hover,
.ef-service-icn {
	color: <?php echo $dark_col; ?>;
}

.bypostauthor .ef-comment-num {
	color: <?php echo $light_col; ?>;
}

.ef-pass,
.woocommerce .products .star-rating,
.woocommerce-page .products .star-rating,
.woocommerce .star-rating:before,
.woocommerce-page .star-rating:before,
.woocommerce .star-rating span:before,
.woocommerce-page .star-rating span:before,
.woocommerce p.stars a,
.woocommerce-page p.stars,
.woocommerce p.stars a.active:after,
.woocommerce p.stars a:hover:after,
.woocommerce-page p.stars a.active:after,
.woocommerce-page p.stars a:hover:after {
	color: <?php echo $main_col; ?>;
}

.bg-primary,
.btn-primary,
fieldset[disabled] .btn-primary.active,
.btn-primary .badge,
.progress-bar,
a.list-group-item.active,
a.list-group-item.active:hover,
a.list-group-item.active:focus,
.nav-pills > li.active > a,
.nav-pills > li.active > a:hover,
.nav-pills > li.active > a:focus,
.label-primary,
.ef-service > i,
.ef-author-posts,
.ef-sticky-icon,
.ef-post-edit-link:hover,
.ef-comment-edit-link:hover,
.mejs-controls .mejs-time-rail .mejs-time-current,
.ef-nextpage > .ef-current-page-num,
.woocommerce a.added_to_cart,
.woocommerce-page a.added_to_cart,
.woocommerce #content input.button.alt,
.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt,
.woocommerce-page #content input.button.alt,
.woocommerce-page #respond input#submit.alt,
.woocommerce-page a.button.alt,
.woocommerce-page button.button.alt,
.woocommerce-page input.button.alt,
.woocommerce #content div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce table.shop_table > thead,
.woocommerce-page table.shop_table > thead,
#ef-woo-shopping-cart a.cart-contents > span,
#ef-woo-shopping-cart a.button.checkout,
.widget_recent_entries ul li:before,
.ef-progress-bar div div {
	background-color: <?php echo $main_col; ?>;
}

.form-control:focus,
a.list-group-item.active,
a.list-group-item.active:hover,
a.list-group-item.active:focus,
.btn-primary,
#ef-contact-form button[type="submit"]:hover,
#ef-comment-form button[type="submit"]:hover,
.nav .open > a,
.nav .open > a:hover,
.nav .open > a:focus,
.form-submit:hover,
.top-bar-section ul li:not(.active) > a:after,
.top-bar-section ul li > a:before,
.ef-posted-in li a:hover:before,
.ef-posted-on li a:hover:before,
#ef-portfolio-filter li.cbp-filter-item-active > a:before,
#ef-list-categories a:hover:after,
.pagination > li > a:after,
.pagination > li > span.current:after,
.btn.btn-default:hover,
.tagcloud li a:hover,
.comment-reply-link:hover,
.ef-underlined-title.text-center:after,
#ef-footer-inner:before,
#ef-blog-inner:not(.ef-min-blog) .ef-post.type-post .ef-proj-desc:after,
.ef-gallery-outer .wp-caption-text.gallery-caption:after,
.woocommerce #content input.button:hover,
.woocommerce #respond input#submit:hover,
.woocommerce a.button:hover,
.woocommerce button.button:hover,
.woocommerce input.button:hover,
.woocommerce-page #content input.button:hover,
.woocommerce-page #respond input#submit:hover,
.woocommerce-page a.button:hover,
.woocommerce-page button.button:hover,
.woocommerce-page input.button:hover,
.woocommerce a.added_to_cart,
.woocommerce-page a.added_to_cart,
.woocommerce #content input.button.alt,
.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt,
.woocommerce-page #content input.button.alt,
.woocommerce-page #respond input#submit.alt,
.woocommerce-page a.button.alt,
.woocommerce-page button.button.alt,
.woocommerce-page input.button.alt,
.woocommerce .related > h2:after,
.woocommerce .related > h2:after,
.woocommerce-page .related > h2:after,
.woocommerce-page .related > h2:after,
.product_meta a:hover:after,
#ef-woo-shopping-cart a.button.checkout,
input.wpcf7-submit[type="submit"]:hover,
.panel.panel-default {
	border-color: <?php echo $main_col; ?>;
}

.woocommerce #order_review_heading:after,
.woocommerce-page #order_review_heading:after {
	border-top-color: <?php echo $main_col; ?>;
}

::selection {
	background: <?php echo $main_col; ?>;
}

::-moz-selection {
	background: <?php echo $main_col; ?>;
}

/* End main colors */

<?php if ( $is_dark ) { ?>
/* White text on dark background */

.ef-post-edit-link:hover > a,
.ef-post-edit-link:hover,
.btn-primary,
.btn-primary:hover,
.btn-primary:focus,
.ef-comment-edit-link:hover,
.ef-sticky-icon,
a.ef-author-posts,
a.ef-author-posts:hover,
a.ef-author-posts:focus,
#ef-woo-shopping-cart a.cart-contents > span,
.ef-nextpage > .ef-current-page-num,
.woocommerce a.added_to_cart,
.woocommerce-page a.added_to_cart,
.woocommerce #content input.button.alt,
.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt,
.woocommerce-page #content input.button.alt,
.woocommerce-page #respond input#submit.alt,
.woocommerce-page a.button.alt,
.woocommerce-page button.button.alt,
.woocommerce-page input.button.alt,
#ef-woo-shopping-cart a.button.checkout,
.woocommerce table.shop_table > thead,
.woocommerce-page table.shop_table > thead,
.woocommerce #content div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce-page #content div.product .woocommerce-tabs ul.tabs li.active a,
.woocommerce-page div.product .woocommerce-tabs ul.tabs li.active a,
.widget_recent_entries ul li > a:hover {
	color: #fff;
}

::selection {
	color: #fff;
}

::-moz-selection {
	color: #fff;
}
<?php } ?>

<?php if ( !empty( $options['ef_heading_font']['font-family'] ) ) { ?>
h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6, #ef-welcome-block #ef-main-description, #ef-list-categories a, .top-bar-section ul li > a, #ef-portfolio-filter li > a, .comment .ef-post-author a > strong, #sb-title-inner, .ef-comment-num, #ef-project-details header, .ef-share-title, .woocommerce #content div.product .woocommerce-tabs ul.tabs li a, .woocommerce div.product .woocommerce-tabs ul.tabs li a, .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li a, .woocommerce-page div.product .woocommerce-tabs ul.tabs li a, .woocommerce table.shop_table > thead, .woocommerce-page table.shop_table > thead, .woocommerce #content div.product p.price, .woocommerce #content div.product span.price, .woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce-page #content div.product p.price, .woocommerce-page #content div.product span.price, .woocommerce-page div.product p.price, .woocommerce-page div.product span.price, .woocommerce ul.products li.product .price, .woocommerce-page ul.products li.product .price, .woocommerce .shop_table .product-name a, .woocommerce-page .shop_table .product-name a, .wp-caption > .wp-caption-text {
	font-family: <?php echo $options['ef_heading_font']['font-family']; ?>;
	<?php if ( !empty( $options['ef_heading_font']['font-weight'] ) ) { ?>
	font-weight: <?php echo $options['ef_heading_font']['font-weight']; ?>;
	<?php } ?>
	<?php if ( !empty( $options['ef_heading_font']['font-style'] ) ) { ?>
	font-style: <?php echo $options['ef_heading_font']['font-style']; ?>;
	<?php } ?>
}
<?php } ?>

<?php if ( !empty( $options['ef_heading_font']['font-weight'] ) ) { ?>
.ef-empty-desc #ef-welcome-block #ef-main-title {
	font-weight: <?php echo $options['ef_heading_font']['font-weight']; ?>;
}
<?php } ?>

h1, h2, h3, h4, h5, .h1, .h2, .h3, .h4, .h5, .top-bar-section ul li > a, #ef-welcome-block #ef-main-description, .ef-empty-desc #ef-welcome-block #ef-main-title {
    letter-spacing: <?php echo !empty( $options['ef_heading_font']['letter-spacing'] ) ? $options['ef_heading_font']['letter-spacing'] : 0; ?>;
}

<?php if ( !empty( $options['ef_content_font']['font-family'] ) ) { ?>
body, blockquote small, blockquote .small, .ef-post .ef-additional-info, #ef-team > article.type-team header > h1 > small {
	font-family: <?php echo $options['ef_content_font']['font-family'] ?>;
}
<?php } ?>

<?php if ( !empty( $options['ef_deco_font']['font-family'] ) ) { ?>
blockquote, q, #ef-main-description em, .ef-fancy {
	font-family: <?php echo $options['ef_deco_font']['font-family'] ?>;
}
<?php } ?>

<?php if ( !empty( $options['ef_custom_css'] ) ) { ?>
	/* Custom CSS */

	<?php echo $options['ef_custom_css']; ?>
<?php } ?>