<?php

/**
 * Generate alerts if have no posts (outputs while user is logged in)
 *
 * @since Brama 1.0
 */
function ef_get_msg_data( $get_posttype = '', $ef_data = '' ) {
	if ( is_user_logged_in() ) {
		if ( current_user_can( 'edit_posts' ) ) {
			if ( empty( $get_posttype ) || ( !$ef_data['cpt_support'] && $get_posttype !== 'post' ) ) {
				$output = __( 'You need to install and activate "Fireform CPT - Brama" plugin to display custom post types', EF_TDM );
			} else {
				$link = sprintf( __( ' Ready to publish your post? <a href="%s"><abbr title="Add new">Get started here</abbr></a>', EF_TDM ), admin_url( 'post-new.php' ) . '?post_type=' . $get_posttype . '' );

				$posts_total = $ef_data['cpt_support'] ? wp_count_posts( EF_CPT ) : false;

				if ( $posts_total && $get_posttype === EF_CPT && $posts_total->publish > 0 ) {
					$output = __( 'Nothing to show. Assign "Featured Image" to each post, please.', EF_TDM );
				} else {
					$pt = $get_posttype == 'post' ? $get_posttype.'s' : $get_posttype;
					$output = sprintf( __( 'Add at least one post to "%1s".%2s', EF_TDM ), ucfirst( $pt ), $link );
				}
			}
		} else {
			$output = __( 'The current user can\'t edit posts. Log in as an administrator.', EF_TDM );
		}

		if ( !empty( $output ) ) {
			printf( '<div class="ef-admin-message alert alert-info">%s</div>', $output );
		}
	}
}