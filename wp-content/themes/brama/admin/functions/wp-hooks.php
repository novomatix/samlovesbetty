<?php

/**

  Add custom body classes
  @since	Brama 1.0

 * */
add_filter( 'body_class', 'ef_theme_body_classes_68163' );
function ef_theme_body_classes_68163( $classes ) {
	$ef_data = EfGetOptions::TplConditions();

	$ef_id = get_current_blog_id();
	$classes[] = 'site-id-'.$ef_id;

	if ( ( is_404() || ( $ef_data['post_id'] && post_password_required( $ef_data['post_id'] ) && !( is_single() && !has_post_thumbnail( $ef_data['post_id'] ) ) ) || EfPrintImages::is_dark_first_mage( $ef_data, $ef_data['post_id'] ) ) && !is_attachment( $ef_data['post_id'] ) ) {
		//$classes[] = 'ef-dark-adjustor';
	}

	if ( is_page() && post_password_required( $ef_data['post_id'] ) ) {
		$classes[] = 'ef-protected-page';
	}

	if ( $ef_data['post_id'] && ( ( is_single() && !has_post_thumbnail() ) || ( current_theme_supports( 'woocommerce' ) && is_product() ) || is_attachment() ) ) {
		$classes[] = 'ef-no-header';
	}

	if ( isset( $ef_data['portf_style'] ) ) {
		$classes[] = $ef_data['portf_style'];
	}

	if ( isset( $ef_data['home_style'] ) ) {
		$classes[] = $ef_data['home_style'];
	}

	$slides = EfPrintImages::slides( $ef_data );

	if ( count( $slides ) == 1 || array_key_exists( 'video', $slides ) ) {
		$classes[] = 'ef-one-slide';
	}

	if ( $ef_data['parent_link'] || ( $ef_data['has_map'] && is_page_template( 'templates/contact-template.php' ) ) ) {
		$classes[] = 'ef-has-parent-link';
	}

	if ( !term_description() && empty( $ef_data['page_desc'] ) && !is_author() && !( is_front_page() && is_home() ) && !is_single() ) {
		$classes[] = 'ef-empty-desc';
	}

	if ( ( ( is_page_template( 'templates/blog-template.php' ) || is_category() || is_tag() || is_home() || is_author() || is_archive() || is_search() || is_404() ) && !is_post_type_archive() && !is_tax() && !is_single() ) || ( !is_page_template( 'templates/minimal-template.php' ) && post_password_required() && has_post_thumbnail() ) || ( current_theme_supports( 'woocommerce' ) && ( ( is_woocommerce() && !is_product() ) || is_cart() || is_checkout() ) ) ) {
		$classes[] = 'ef-advanced-layout';
	}

	return $classes;
}


/**

  Portfolio post custom classes
  @since	Brama 1.0

* */
function ef_portfolio_post_class_68163( $classes ) {
	global $post;

	if ( !has_post_thumbnail( $post->ID ) ) {
		$classes[] = 'no-post-thumbnail';
	}

	if ( $post->post_type === 'portfolios' && !is_singular( 'portfolios' ) ){
		if ( current_theme_supports( 'ef-meta-boxes' ) ) {
			$thumb = efmb_meta( 'ef_img_orientation', 'type=radio' );
			$classes[] = $thumb === '1' ? 'cbp-height2x' : '';
		}
		$terms = get_the_terms( $post->ID, EF_CPT_TAX_CAT );
		$classes[] = 'ef-post';
		$classes[] = 'cbp-item';

		if( !empty( $terms ) ) {
			foreach( (array) $terms as $term ) {
				if( !in_array( $term->term_id, $classes ) ) {
					$classes[] = "cbp-filter-" . $term->term_id;
				}
			}
		}
	}

	return $classes;
}
add_filter( 'post_class', 'ef_portfolio_post_class_68163', 10, 2 );


/**

  Add shortcodes support
  @since	Brama 1.0

* */
add_filter( 'widget_text', 'do_shortcode' );


/**

  Customizing excerpt
  @since	Brama 1.0

* */
function ef_theme_excerpt_length_68163( $length ) {
	return 30;
}
add_filter('excerpt_length', 'ef_theme_excerpt_length_68163');

function ef_theme_excerpt_more_68163( $more ) {
	return ' ...';
}
add_filter('excerpt_more', 'ef_theme_excerpt_more_68163');


/**

  Output custom document titles
  @since Brama 1.0

* */
add_filter( 'wp_title', 'ef_theme_filter_wp_title_68163' );
function ef_theme_filter_wp_title_68163( $title = '', $sep = '', $seplocation = '' ) {

	global $paged, $page, $post;
	$ef_data = EfGetOptions::TplConditions();

	if ( is_feed() ) {
		return $title;
	}

	if ( is_search() ) {
		$title = sprintf( __( 'Search results for %s', EF_TDM ), '"' . get_search_query() . '"' );
		$title .= " $sep " . get_bloginfo( 'name', 'display' );
		if ( $paged >= 2 ) {
			$title .= " $sep " . sprintf( __( 'Page %s', EF_TDM ), $paged );
		}
		return $title;
	}

	$title .= get_bloginfo( 'name', 'display' );

	if ( is_front_page() ) {
		$title .= " | " . get_bloginfo( 'description', 'display' );
	}

	if ( $paged >= 2 || $page >= 2 ) {
		$sep = !is_sticky() && !is_front_page() ? $sep : '';
		$title .= " $sep " . sprintf( __( 'Page %s', EF_TDM ), max( $paged, $page ) );
	}

	return $title;
}


/**

  Prev/next post links
  @since Brama 1.0

* */
add_filter('previous_post_link', 'ef_posts_link_prev_class');
function ef_posts_link_prev_class($format) {
	$format = str_replace( 'href=', 'id="ef-prev-project" href=', $format );
	return $format;
}

add_filter( 'next_post_link', 'ef_posts_link_next_class' );
function ef_posts_link_next_class($format){
	$format = str_replace( 'href=', 'id="ef-next-project" href=', $format );
	return $format;
}


/**

  Comment form hooks
  @since Brama 1.0

* */
add_filter( 'comment_form_default_fields', 'ef_comment_form_default_fields_68163' );
function ef_comment_form_default_fields_68163( $fields ) {

	$commenter  = wp_get_current_commenter();
	$req 		= get_option( 'require_name_email' );
	$aria_req 	= ( $req ? " aria-required='true'" : '' );
	if ( isset( $fields['url'] ) ) {
		unset( $fields['url'] );
	}

	$fields['author'] = '<div class="form-group"><input placeholder="' . __( 'Name *', EF_TDM ) . '" class="form-control" name="author" type="text" size="30" tabindex="1"' . $aria_req . ' value="' . esc_attr( $commenter['comment_author'] ) . '" /></div>';

	$fields['email'] = '<div class="form-group ef-block-merger"><input placeholder="' . __( 'E-mail *', EF_TDM ) . '" class="form-control" name="email" type="text" size="30" tabindex="2"' . $aria_req . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" /></div>';

	return $fields;
}


add_filter( 'comment_form_field_comment', 'ef_comment_form_field_comment_68163' );
function ef_comment_form_field_comment_68163( $comment_field ) {

	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	if ( !( current_theme_supports( 'woocommerce' ) && is_product() ) ) {
		$comment_field = '<div class="ef-textarea row"><div class="col-lg-12"><div class="form-group"><textarea placeholder="' . __( 'Message *', EF_TDM ) . '" class="form-control" rows="5" name="comment" type="text" tabindex="3" ' . $aria_req . '></textarea></div></div></div>';
	}
	return $comment_field;
}

add_action( 'comment_form_before_fields', 'ef_add_fields_before_68163' );
function ef_add_fields_before_68163() {
	echo '<div class="row"><div class="col-md-12">';
}

add_action( 'comment_form_after_fields', 'ef_add_fields_after_68163' );
function ef_add_fields_after_68163() {
	echo '</div></div>';
}

// Add comments support
add_action( 'init', 'ef_add_comments_support_68163', 11 );
function ef_add_comments_support_68163() {
	global $efto_data;
	if ( defined( 'EF_CPT' ) && isset( $efto_data['ef_portf_comments'] ) && !empty( $efto_data['ef_portf_comments'] ) ) {
		add_post_type_support( EF_CPT, 'comments' );
	}
}


/**

  Searchbox form
  @since Brama 1.0

* */
add_filter( 'get_search_form', 'ef_custom_search_form_68163' );
function ef_custom_search_form_68163( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="form-inline ef-searchform" action="' . esc_url( home_url( '/' ) ) . '" >
		<div class="form-group">
			<input type="text" class="form-control" value="' . get_search_query() . '" name="s" id="s" placeholder="'. esc_attr( __( 'Type your keywords...', EF_TDM ) ) .'" />
		</div>
		<div class="form-group">
			<input type="submit" class="btn btn-primary" id="searchsubmit" value="'. esc_attr( __( 'Go', EF_TDM ) ) .'" />
		</div>
    </form>';

	return $form;
}


/**

  Password form
  @since Brama 1.0

* */
add_filter( 'the_password_form', 'ef_custom_password_form_68163' );
function ef_custom_password_form_68163( $output ) {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	if ( !empty( $post->ID ) ) {
		switch ( $post->post_type ) {
			case 'page':
				$page = 'page';
				break;
			case 'portfolios':
				$page = 'project';
				break;
			default:
				$page = 'post';
				break;
		}
	} else {
		$page = 'post';
	}

	$cnter = $page === 'page' || is_single() ? ' text-center' : '';
	$output = '<form class="ef-pass-form' . $cnter . ' form-inline" action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post"><p>' . sprintf(__( "To view this protected %s, enter the password below:", EF_TDM ), $page) . '</p><div class="form-group"><input class="form-control" name="post_password" id="' . esc_attr( $label ) . '" type="password" /></div><div class="form-group"><input class="btn btn-primary" type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" /></div></form>';
	return $output;
}


/**

  Replacing the no excerpt text with a password form
  @since Brama 1.0

* */
add_filter( 'the_excerpt', 'ef_excerpt_post_protected_68163' );
function ef_excerpt_post_protected_68163( $content ) {
	if ( post_password_required() ) {
		$content = get_the_password_form();
	}
	return $content;
}


/**

  Allowing SVG uploads
  @since Brama 1.0

* */
add_filter( 'upload_mimes', 'ef_custom_upload_mimes_68163' );
function ef_custom_upload_mimes_68163( $existing_mimes = array() ) {
	$existing_mimes['svg'] = 'mime/type';
	return $existing_mimes;
}


/**

  Customizing RSS feed (adding images)
  @since Brama 1.0

* */
add_filter( 'the_excerpt_rss', 'ef_custom_rss_view_68163' );
add_filter( 'the_content_feed', 'ef_custom_rss_view_68163' );
function ef_custom_rss_view_68163( $content ) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ) {
		$content = '<p>' . get_the_post_thumbnail( $post->ID, 'medium_thumb' ) . '</p>' . get_the_excerpt();
	}
	return $content;
}


/**

  Portfolios to RSS feed
  @since Brama 1.0

* */
function ef_add_portfolios_rss_view_68163( $query ) {
    if ( $query->is_feed() ) {
    	$query->set( 'post_type', array( 'post', 'portfolios' ) );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'ef_add_portfolios_rss_view_68163' );


/**

  Disabling comments on WordPress media attachments
  @since Brama 1.0

* */
add_filter( 'comments_open', 'ef_attachment_comment_status_68163' );
function ef_attachment_comment_status_68163( $open ) {
	global $post;
	$post = get_post( $post->ID );
	if ( $post->post_type == 'attachment' ) {
		return false;
	}
	return $open;
}

/**

  Adding default title if it isn't specified
  @since Brama 1.0

* */
add_filter( 'the_title', 'ef_intitled_post_68163' );
function ef_intitled_post_68163( $title ) {
	if ( $title == '' ) {
		return apply_filters( 'ef_default_post_title', __( 'Untitled', EF_TDM ) );
	} else {
		return $title;
	}
}


/**

  get_comment_author_link
  @since Brama 1.0

* */
add_filter( "get_comment_author_link", "ef_modifiy_comment_author_anchor_68163" );
function ef_modifiy_comment_author_anchor_68163( $author_link ){
    return str_replace( "<a", "<a target='_blank'", $author_link );
}


/**

  Enqueue comment-reply script
  @since Brama 1.0

* */
add_action( 'comment_form_before', 'ef_theme_enqueue_comment_reply_68163' );
function ef_theme_enqueue_comment_reply_68163() {
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}


/**

  Fixing empty search
  @since Brama 1.0

* */
function ef_fix_blank_search_68163( $query ) {
	global $wp_query;
	if ( isset( $_GET['s'] ) && ( $_GET['s'] == '' ) ) {
		$wp_query->set( 's', ' ' );
		$wp_query->is_search = true;
	}
	return $query;
}

add_action( 'pre_get_posts', 'ef_fix_blank_search_68163' );


/**

  Widget Tagcloud
  @since Brama 1.0

* */
function ef_custom_tag_cloud_widget_68163( $args ) {
	$args['format'] = 'list';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'ef_custom_tag_cloud_widget_68163' );


/**

  Contact form 7 tweaks
  @since Brama 1.0

* */
add_action( 'wp_enqueue_scripts', 'wap8_wpcf7_css', 10 );
function wap8_wpcf7_css() {
	if ( !is_admin() && class_exists( 'WPCF7_ContactForm' ) ) {
		wp_deregister_style( 'contact-form-7' );
	}
}


/**

  Customizing feature support
  @since Brama 1.0

* */
add_action( 'admin_init', 'remove_post_type_support_68163' );
function remove_post_type_support_68163() {

	$page_tpls = array(
		'templates/home-template.php',
		'templates/contact-template.php',
	);

	$page_tpls1 = array(
		'templates/home-template.php'
	);

	if ( isset ( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	} else if ( isset ( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}

	if ( !isset ( $post_id ) || empty ( $post_id ) ) {
		return;
	}

	$template_file = get_post_meta( $post_id, '_wp_page_template', true );

	if ( in_array( $template_file, $page_tpls ) ) {
		remove_post_type_support( 'page', 'thumbnail' );
	}

	if ( in_array( $template_file, $page_tpls1 ) ) {
		remove_post_type_support( 'page', 'editor' );
	}
}


/**

  Current post parents
  @since Brama 1.0

* */
function ef_single_parent_nav_class_68163( $classes = array(), $item = false ) {
	$ef_data = EfGetOptions::TplConditions();

	if ( $ef_data['parent_link'] ) {
		$item_id = get_post_meta( $item->ID, '_menu_item_object_id', true );
		if ( $item_id === $ef_data['parent_link']['post_id'] ) {
			$classes[] = 'active';
		}
	}

	return $classes;
}
add_filter( 'nav_menu_css_class', 'ef_single_parent_nav_class_68163', 10, 2 );


/**

  Add Facebook Open Graph and Twitter Card Meta Tags To WordPress
  @since Brama 1.0

* */
function ef_sharing_meta_head_68163() {
	$ef_data = EfGetOptions::TplConditions();

	if ( !is_feed() && !is_search() && !is_404() && !is_archive() && !post_password_required() && $ef_data['to_support'] ) {
		global $post;

		$slides = EfPrintImages::slides( $ef_data );

		if ( is_page_template( 'templates/home-template.php' ) && count( $slides ) > 0 ) {
			$default_image = $slides[0]['thumbs']['share_thumb'];
		} elseif ( $post !== '' && has_post_thumbnail( $post->ID ) ) {
			$thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'share_thumb' );
			$default_image = esc_attr( $thumbnail_src[0] );
		} else {
			$default_image = get_template_directory_uri() . '/assets/img/share-on-fb.jpg';
		}

		$the_excerpt = wp_trim_words( strip_shortcodes( $post->post_content ) );

		if ( is_single() ) {
			echo '<meta property="og:type" content="article" />';
		}

		$title = !is_sticky() && !is_front_page() ? esc_attr( get_the_title() ) : get_bloginfo( 'name' );
		$desc = !empty( $the_excerpt ) ? $the_excerpt : get_bloginfo( 'description' );

		printf( '<meta property="og:title" content="%s" />
			<meta property="og:image" content="%s" />
			<meta property="og:description" content="%s" />
			<meta property="og:url" content="%s" />
			<meta property="og:site_name" content="%s" />
			<link rel="image_src" href="%s" />',
			esc_attr( $title ),
			$default_image,
			esc_html( $desc ),
			esc_url( get_permalink() ),
			get_bloginfo( 'name' ),
			esc_url( $default_image )
		);

		if ( isset( $ef_data['ef_twt_username'] ) ) {
			echo '<meta name="twitter:site" content="'.$ef_data['ef_twt_username'].'">
				<meta name="twitter:creator" content="'.$ef_data['ef_twt_username'].'">';
		}

		printf( '<meta name="twitter:card" content="summary">
			<meta name="twitter:url" content="%s">
			<meta name="twitter:title" content="%s">
			<meta name="twitter:description" content="%s">
			<meta name="twitter:image" content="%s">',
			esc_url( get_permalink() ),
			esc_attr( $title ),
			esc_html( $desc ),
			$default_image
		);
	}
}
add_action( 'wp_head', 'ef_sharing_meta_head_68163', 5 );


/**

  Filtering wp_link_pages to wrap current page in span.
  @since Brama 1.0

 * */
function ef_link_pages_current_68163( $link ) {
    if ( ctype_digit( $link ) ) {
        return '<span class="ef-current-page-num">' . $link . '</span>';
    }
    return $link;
}
add_filter( 'wp_link_pages_link', 'ef_link_pages_current_68163' );


/**

  Disable comments on pages by default.
  @since Brama 1.0

 * */
function ef_default_page_comments_off_68163( $data ) {
    if( $data['post_type'] == 'page' && $data['post_status'] == 'auto-draft' ) {
        $data['comment_status'] = 0;
    }

    return $data;
}
add_filter( 'wp_insert_post_data', 'ef_default_page_comments_off_68163' );


/**

  Enable Vimeo API for oEmbed.
  @since Brama 1.0

 * */
function ef_oembed_enable_api_68163( $provider, $url, $args ) {
	if ( strpos( $provider, 'vimeo.com' ) ) {
		$provider = add_query_arg( array(
			'api' => '1',
			'player_id' => 'ef-ext-video-v'
		), $provider );
	}
	return $provider;
}
add_filter( 'oembed_fetch_url', 'ef_oembed_enable_api_68163', 10, 3 );