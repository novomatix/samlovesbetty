<?php

// Main functions
require_once ( 'functions/functions.php' );

// WooCommerce addon
if ( class_exists( 'Woocommerce' ) && ef_is_plugin_active( 'woocommerce/woocommerce.php' ) && file_exists( get_template_directory() . '/woocommerce/functions.php' ) ) {
	require_once get_template_directory() . '/woocommerce/functions.php';
}

// TGM Plugin Activation class
require_once ( 'classes/class-tgm-plugin-activation.php' );

// Load options class
require_once( 'classes/class.conditions.php' );

// Header images (+slideshow)
require_once( 'classes/class.mediacore.php' );

// Comments Walker
require_once( 'classes/class.comments.walker.php' );

// WP filters
require_once ( 'functions/wp-hooks.php' );

// Helpers
require_once ( 'functions/messages.php' );

// Stylesheets + scripts
require_once ( 'main/enqueue.php' );

// Sidebars
require_once ( 'main/sidebars.php' );

// Menus
require_once ( 'main/menus.php' );