<?php

/**
 * Register custom sidebars.
 *
 * @since Brama 1.0
 * @uses register_sidebar
 */
function ef_register_sidebars_8973892() {

	/**
	 * Register custom sidebars.
	 */

	if ( current_theme_supports( 'ef-custom-post-types' ) ) {

		$args = array(
	        'post_type' => EF_CPT_SIDEBARS,
	        'post_status' => 'publish',
	        'posts_per_page' => -1
        );

		$sb_query = new WP_Query( $args );
		$sidebars_array = $sb_query->posts;
		wp_reset_query();
		$k = null;

		if ( !empty( $sidebars_array ) ) {
		    foreach( $sidebars_array as $k ) {
		        register_sidebar( array(
		            'name'			=> $k->post_title,
		            'description'	=> __( 'Custom widget area', EF_TDM ),
		            'id'			=> 'ef-sidebar-' . $k->ID,
		            'before_widget'	=> '<section id="%1$s" class="ef-widget %2$s col-lg-3 col-md-6"><div class="widget">',
					'after_widget'	=> "</div></section>",
					'before_title'	=> '<h4>',
					'after_title'	=> '<span></span></h4>',
		        ) );
		    }
		}
	}


	/**
	 * Register default sidebars.
	 */

	register_sidebar( array(
		'name'			=> __( 'Sidebar widget area', EF_TDM ),
		'id'			=> 'ef-sidebar-widget-area',
		'description'	=> __( 'Default widget area', EF_TDM ),
		'before_widget' => '<section id="%1$s" class="ef-widget %2$s"><div class="widget">',
		'after_widget'	=> "</div></section>",
		'before_title'	=> '<h4 class="ef-widget-title">',
		'after_title'	=> '<span></span></h4>',
	) );

	register_sidebar( array(
		'name'			=> __( 'Footer widget area', EF_TDM ),
		'id'			=> 'ef-footer-widget-area',
		'description'	=> __( 'Default widget area', EF_TDM ),
		'before_widget' => '<section id="%1$s" class="ef-widget %2$s col-lg-3 col-md-6"><div class="widget">',
		'after_widget'	=> "</div></section>",
		'before_title'	=> '<h4 class="ef-widget-title">',
		'after_title'	=> '<span></span></h4>',
	) );

	register_sidebar( array(
		'name'			=> __( 'Menu widget area', EF_TDM ),
		'id'			=> 'ef-menu-widget-area',
		'description'	=> __( 'Default widget area', EF_TDM ),
		'before_widget' => '<section id="%1$s" class="ef-widget %2$s col-lg-3 col-md-6"><div class="widget">',
		'after_widget'	=> "</div></section>",
		'before_title'	=> '<h4 class="ef-widget-title">',
		'after_title'	=> '<span></span></h4>',
	) );

	register_sidebar( array(
		'name'			=> __( 'Account widget area', EF_TDM ),
		'id'			=> 'ef-account-widget-area',
		'description'	=> __( 'Default widget area', EF_TDM ),
		'before_widget' => '<section id="%1$s" class="ef-widget %2$s"><div class="widget jq-dropdown-menu">',
		'after_widget'	=> "</div></section>",
		'before_title'	=> '<h4 class="ef-widget-title">',
		'after_title'	=> '<span></span></h4>',
	) );
}

add_action( 'widgets_init', 'ef_register_sidebars_8973892' );