<?php

/*
 * Theme Stylesheets
 */

add_action( 'wp_enqueue_scripts', 'ef_theme_enqueue_css' );

function ef_theme_versn() {
	$theme_data = wp_get_theme();
	return $theme_data->get( 'Version' );
}

function ef_theme_enqueue_css() {

	$ef_data = EfGetOptions::TplConditions();
	$template_url = get_template_directory_uri();
	$themeVer = ef_theme_versn();
	$uploads_dir = wp_upload_dir();
	$ef_css_url = trailingslashit( $uploads_dir['baseurl'] );
	$ef_css_dir = trailingslashit( $uploads_dir['basedir'] );
	$if_options_css = file_exists( $ef_css_dir . 'wp_brama_style.css' );

	/*--- Google fonts stylesheets (enqueue if Redux Framework isn't active) ---*/

	if ( !$ef_data['to_support'] ) {
		wp_enqueue_style( 'ef-googleFonts-head', '//fonts.googleapis.com/css?family=Montserrat:400,700', false, $themeVer, 'screen' );
		wp_enqueue_style( 'ef-googleFonts-content', '//fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic', false, $themeVer, 'screen' );
		wp_enqueue_style( 'ef-googleFonts-serif', '//fonts.googleapis.com/css?family=Libre+Baskerville:400,700,400italic', false, $themeVer, 'screen' );
	}

	/*---------------------------------*/

	if ( !wp_script_is( 'wp-mediaelement', 'done' ) ) {
		wp_enqueue_style( 'wp-mediaelement' );
	}

	if ( $ef_data['to_support'] && $ef_data['ef_minifier'] ) {
		wp_enqueue_style( 'ef-theme', $template_url . '/assets/css/theme.min.css', array(), $themeVer, 'all' );
	} else {
		wp_enqueue_style( 'ef-bootstrap', $template_url . '/assets/css/bootstrap.min.css', array(), $themeVer, 'all' );
		wp_enqueue_style( 'ef-topbar', $template_url . '/assets/css/topbar.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-fontello', $template_url . '/inc/fontello/css/fontello.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-fontello-animation', $template_url . '/inc/fontello/css/animation.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-taras', $template_url . '/inc/taras-font/css/styles.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-itcavantgardepro', $template_url . '/inc/itcavantgardepro/css/styles.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-shadowbox', $template_url . '/assets/css/shadowbox.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-cubeportfolio', $template_url . '/assets/css/cubeportfolio.min.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-general', $template_url . '/assets/css/style.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-color', $template_url . '/assets/css/colors.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-superfish', $template_url . '/assets/css/superfish.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-superfish-navbar', $template_url . '/assets/css/superfish-navbar.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-owl-carousel', $template_url . '/assets/css/owl.carousel.css', array(), $themeVer, 'screen' );
		wp_enqueue_style( 'ef-dropdown', $template_url . '/assets/css/jquery.dropdown.css', array(), $themeVer, 'screen' );
	}

	wp_enqueue_style( 'ef-style', get_stylesheet_uri(), array(), $themeVer, 'screen' );

	if ( $ef_data['to_support'] ) {
		if ( $if_options_css ) {
			wp_enqueue_style( 'ef-options', $ef_css_url . 'wp_brama_style.css', array(), $themeVer, 'screen' );
		} else {
			wp_enqueue_style( 'ef-options', admin_url( 'admin-ajax.php' ) . '?action=ef_dynamic_css_68163', array(), $themeVer, 'screen' );
		}
	}
}


/*
 * Enqueue scripts
 */

add_action( 'wp_enqueue_scripts', 'ef_theme_enqueue_to_head', -1 );
add_action( 'wp_enqueue_scripts', 'ef_theme_enqueue_scripts' );

function ef_theme_enqueue_to_head() {
	$template_url = get_template_directory_uri();
	wp_enqueue_script( 'ef-modernizr', $template_url . '/assets/js/modernizr.custom.js', array(), '', false );
}

function ef_theme_enqueue_scripts() {

	$template_url = get_template_directory_uri();
	$themeVer = ef_theme_versn();
	$ef_data = EfGetOptions::TplConditions();

	global $wp_scripts;

	if ( !wp_script_is( 'jquery', 'done' ) ) {
		wp_enqueue_script( 'jquery' );
	}

	wp_enqueue_script( 'jquery-masonry' );

	if ( !wp_script_is( 'wp-mediaelement', 'done' ) ) {
		wp_enqueue_script( 'wp-mediaelement' );
	}

	if ( is_page_template( 'templates/contact-template.php' ) ) {
		wp_enqueue_script( 'ef-googleMaps', '//maps.google.com/maps/api/js?sensor=false', array(), $themeVer, false );
	}

	if ( $ef_data['to_support'] && $ef_data['ef_minifier'] ) {
		wp_enqueue_script( 'ef-theme-js', $template_url . '/assets/js/theme.min.js', array( 'jquery' ), $themeVer, true );
		wp_localize_script( 'ef-theme-js', 'ef_js_vars', $ef_data['js_vars'] );
	} else {
		wp_enqueue_script( 'ef-owl-carousel', $template_url . '/assets/js/owl.carousel.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-superfish', $template_url . '/assets/js/superfish.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-easing', $template_url . '/assets/js/jquery.easing.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-detectmobilebrowser', $template_url . '/assets/js/detectmobilebrowser.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-froogaloop', $template_url . '/assets/js/froogaloop2.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-bootstrap', $template_url . '/assets/js/bootstrap.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-foundation', $template_url . '/assets/js/foundation.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-transit', $template_url . '/assets/js/jquery.transit.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-cubeportfolio', $template_url . '/assets/js/jquery.cubeportfolio.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-smartresize', $template_url . '/assets/js/smartresize.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-imagesloaded', $template_url . '/assets/js/imagesloaded.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-flexslider', $template_url . '/assets/js/jquery.flexslider.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-dropdown', $template_url . '/assets/js/jquery.dropdown.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-shadowbox', $template_url . '/assets/js/shadowbox/shadowbox.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-efparallax', $template_url . '/assets/js/paralla.min.js', array( 'jquery' ), $themeVer, true );
		wp_enqueue_script( 'ef-app', $template_url . '/assets/js/app.js', array( 'jquery' ), $themeVer, true );
		wp_localize_script( 'ef-app', 'ef_js_vars', $ef_data['js_vars'] );
	}
}