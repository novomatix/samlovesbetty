<?php
/**
 * The template for displaying Author's archive.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

			<?php if ( have_posts() ) { ?>
				<?php the_post(); ?>
			<?php } ?>
			<?php rewind_posts(); ?>
			<?php get_template_part( 'loop', 'archive' ); ?>

			<?php /* Pagination */ ?>
			<?php ef_theme_pagination( $wp_query->max_num_pages, $ef_data ); ?>

<?php get_footer(); ?>
