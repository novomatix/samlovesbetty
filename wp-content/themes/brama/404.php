<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

		<div id="post-0" class="post error404 text-center not-found">
			<p class="text-center"><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', EF_TDM ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- #post-0 -->

<?php get_footer(); ?>