<?php
/**
 * The loop that displays posts.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

	<?php /* If there are no posts to display, such as an empty archive page */ ?>

	<?php if ( ! have_posts() ) { ?>
		<?php ef_get_msg_data( $wp_query->query_vars['post_type'], $ef_data ); ?>
	<?php } else { ?>
		<section id="ef-blog">
            <div id="ef-blog-inner" class="<?php echo $ef_data['is_template'] ? $ef_data['blog_style'] : 'ef-grid-blog'; ?> clearfix">

            <?php $show_cats = $ef_data['mb_support'] ? efmb_meta( 'ef_show_cats', 'type=checkbox', $ef_data['post_id'] ) : 1; ?>

            	<?php if ( !is_search() && !empty( $show_cats ) ) { ?>
	            	<div class="ef-post">
	            		<div id="ef-list-categories">
			        		<div id="ef-back-cats">
								<?php _e( 'Categories', EF_TDM ); ?>
							</div>

			            	<ul>
								<?php wp_list_categories( array( 'title_li' => '', 'hide_empty' => 1 ) ); ?>
							</ul>
						</div><!-- #ef-list-categories -->
					</div><!-- .ef-post -->
				<?php } ?>

				<?php while ( have_posts() ) : the_post(); ?>
					<?php $show_gal = $ef_data['mb_support'] ? efmb_meta( 'ef_show_gallery', 'type=checkbox', $ef_data['post_id'] ) : false; ?>
					<?php $if_has_more_tag = strpos( $post->post_content, '<!--more-->' ); ?>
					<?php $show_content = isset( $ef_data['show_content'] ) && $ef_data['show_content'] =='ef-content'; ?>
					<?php $pass = post_password_required() ? '<span class="ef-pass ts-icon-lock"></span>' : ''; ?>
					<?php $pt = $post->post_type; ?>

					<?php if ( !( !has_post_thumbnail() && $ef_data['cpt_support'] && ( is_tax( EF_CPT_TAX_CAT ) || is_tax( EF_CPT_TAX_TAG ) || is_post_type_archive( EF_CPT ) ) ) ) { ?>

						<article id="post-<?php the_ID(); ?>" <?php post_class( 'ef-post clearfix' ); ?>>
							<div class="ef-proj-img">
								<?php if ( is_sticky() && !is_archive() && !is_search() ) { ?>
									<i class="icon-ef-pin-outline ef-sticky-icon text-center"></i>
								<?php } ?>

								<?php if ( $ef_data['blog_style'] == 'ef-min-blog' ) { ?>
									<a href="<?php esc_url( the_permalink() ); ?>" class="btn"></a>
								<?php } else { ?>
									<?php if ( has_post_thumbnail() ) { ?>
										<?php if ( is_search() && ( $ef_data['cpt_support'] && $pt == EF_CPT ) ) { ?>
											<?php if ( $ef_data['mb_support'] ) { ?>
												<?php $thumb = efmb_meta( 'ef_img_orientation', 'type=radio' ); ?>
												<?php $thumb = $thumb === '1' ? 'portfolio_portrate' : 'portfolio_landscape'; ?>
											<?php } else { ?>
												<?php $thumb = 'portfolio_landscape'; ?>
											<?php } ?>

											<?php echo get_the_post_thumbnail( $post->ID, $thumb ); ?>

											<h1 class="ef-proj-desc entry-title">
												<?php echo $pass; ?>

												<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
				                                	<span class="ef-details-holder">
					                                	<?php the_title(); ?>
													</span>
				                                </a>
											</h1><!-- .ef-proj-desc -->
										<?php } elseif ( $ef_data['mb_support'] && get_post_format() == 'video' && $pt == 'post' ) { ?>
											<?php $vid = EfPrintImages::video_meta( $post->ID ); ?>
											<?php if ( $vid[4] ) { ?>
												<div class="ef-thumb">
													<div class="ef-responsive-iframe">
														<?php echo wp_oembed_get( $vid[4] ); ?>
													</div>
												</div>
											<?php } else { ?>
												<?php EfPrintImages::video( $vid[1], $vid[2], $vid[3], false, false, false ) ?>
											<?php } ?>
										<?php } else { ?>
											<div class="ef-thumb">
												<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
													<?php echo get_the_post_thumbnail( $post->ID, $ef_data['blog_style'] == 'ef-classic-blog' ? 'blog_classix' : 'medium_thumb' ); ?>

													<?php echo $pass; ?>

													<?php if ( !$show_content && !empty( $show_gal ) ) { ?>
														<?php ef_call_gallery_imgs( $post ); ?>
													<?php } ?>
												</a>
											</div><!-- .ef-thumb -->
										<?php } ?>
									<?php } elseif ( !$show_content && !post_password_required() && !empty( $show_gal ) ) { ?>
										<?php ef_call_gallery_imgs( $post, 'a' ); ?>
									<?php } ?>
								<?php } ?>

								<?php if ( $ef_data['blog_style'] == 'ef-classic-blog' ) { ?>
									<div class="ef-post-side">
								<?php } ?>

								<?php if ( !is_404() && !is_search() && !is_tax() && !is_post_type_archive() ) { ?>
									<?php echo ef_theme_posted_on( $post, $ef_data ); ?>
								<?php } ?>

								<?php if ( $pt == 'post' || $pt == 'page' || ( $ef_data['cpt_support'] && $pt == EF_CPT_EXTR ) ) { ?>
									<h1 class="ef-proj-desc entry-title">
										<a title="<?php the_title_attribute(); ?>" href="<?php esc_url( the_permalink() ); ?>"><?php the_title(); ?></a>
									</h1><!-- .ef-proj-desc -->

									<?php if ( $ef_data['blog_style'] !== 'ef-min-blog' ) { ?>
										<div class="entry-content">
											<?php if ( $show_content && !is_sticky() ) { ?>
												<?php global $more; $more = 0; ?>
												<?php if ( $if_has_more_tag ) { ?>
													<?php the_content( '', FALSE, '' ); ?>
												<?php } else { ?>
													<?php $content = apply_filters( 'the_content', strip_shortcodes( $post->post_content ) ); ?>
													<?php echo $content; ?>
												<?php } ?>
												<?php $more = 1; ?>
											<?php } else { ?>
												<?php the_excerpt(); ?>
											<?php } ?>
										</div><!-- .entry-content -->
									<?php } ?>
								<?php } ?>

								<?php if ( $pt == 'post' && $ef_data['blog_style'] !== 'ef-min-blog' && !post_password_required() ) { ?>
									<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>" class="btn btn-default"><?php _e( 'Keep reading', EF_TDM ); ?></a>
								<?php } ?>

								<?php if ( $ef_data['blog_style'] == 'ef-classic-blog' ) { ?>
									</div>
								<?php } ?>
							</div><!-- .ef-proj-img  -->
						</article><!-- .ef-post  -->
					<?php } ?>
				<?php endwhile; // End the loop. ?>
			</div><!-- #ef-blog-inner -->
		</section><!-- #ef-blog -->
	<?php } ?>