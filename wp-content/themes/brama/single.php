<?php

/**
 * The Template for displaying all single posts.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

	<?php get_template_part( 'loop', 'single' ); ?>

<?php get_footer(); ?>