<?php

/**
 * The Header for the theme.
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php EfGetOptions::init(); ?>
<?php $ef_data = EfGetOptions::TplConditions(); ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- Title  -->
    <title><?php wp_title( '&#124;', true, 'right' ); ?></title>

    <!-- Favicon  -->
    <?php if ( !empty( $ef_data['ef_custom_favicon'] ) ) { ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo esc_url( $ef_data['ef_custom_favicon']['url'] ); ?>" />
    <?php } ?>

    <link rel="profile" href="http://gmpg.org/xfn/11" />

    <link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>" />

    <?php wp_head(); ?>
    <?php if ( is_product() ) { ?>
    <style>
    .woocommerce div.product form.cart .button {
        background-color: <?php echo get_field('color')?>;
    }
    </style>
    <?php }?>
</head>

<body <?php body_class(); ?>>

    <div id="ef-loading-overlay"></div>

    <?php if ( EF_DEMO_CHANGER ) ef_demo_changer(); ?>
    <div id="portrait-notification-overlay"></div>
    <nav id="ef-nav" class="ef-positioner">
        <div id="ef-site-name">
            <div class="navbar-brand">
                <a id="ef-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">

                    <?php if ( isset( $ef_data['custom_logo'] ) && !empty( $ef_data['custom_logo'] ) ) { ?>
                    <img class="ef-default-logo" src="<?php echo $ef_data['custom_logo']; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
                    <?php if ( isset( $ef_data['custom_logo_light'] ) && !empty( $ef_data['custom_logo_light'] ) ) { ?>
                    <img class="ef-white-logo" src="<?php echo $ef_data['custom_logo_light']; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
                    <?php } ?>
                    <?php } else { ?>
                    <img class="ef-default-logo" src="<?php echo get_template_directory_uri() . '/assets/img/logo.svg'; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />

                    <img class="ef-white-logo" src="<?php echo get_template_directory_uri() . '/assets/img/logo-white.svg'; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
                    <?php } ?>
                </a><!-- #ef-logo -->
                <?php if ( !is_404() && !post_password_required() && $ef_data['to_support'] && ef_add_share_btns( $ef_data, esc_attr( get_the_title() ), esc_url( get_permalink() ) ) ) { ?>
                <div id="ef-entry-meta">
                    <?php echo ef_add_share_btns( $ef_data, esc_attr( get_the_title() ), esc_url( get_permalink() ) ); ?>
                </div>
                <?php } ?>
            </div><!-- .navbar-brand  -->

            <div id="ef-header-angle" class="clearfix">
                <span class="ef-header-angle"></span><!-- .ef-header-angle -->
            </div><!-- #ef-header-angle -->

            <?php echo ef_wpml_lang_switcher(); ?>
            <?php if (ICL_LANGUAGE_CODE !=='fr') {?>
            <span id="ef-woo-account" class="woocommerce">
                <?php
                $current_user = wp_get_current_user();
                ?>
                <?php if ( 0 == $current_user->ID ) { ?>
                <a class="button" href="<?php echo get_page_link(icl_object_id( 593, 'page', true, ICL_LANGUAGE_CODE )); ?>"><?php  _e( 'Login', EF_TDM ); ?></a>
                <?php } else { ?>
                <a class="button right" data-jq-dropdown="#jq-dropdown-1"><?php _e( 'My account', EF_TDM ); ?></a>
                <div id="jq-dropdown-1" class="jq-dropdown jq-dropdown-tip jq-dropdown-anchor-right">
                    <?php if ( is_active_sidebar( 'ef-account-widget-area' ) ) { ?>
                    <?php dynamic_sidebar( 'ef-account-widget-area'  ); ?>
                    <?php } ?>
                </div>
                <?php } ?>
            </span>
            <?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' ); ?>
            <?php if ( current_theme_supports( 'woocommerce' ) && is_plugin_active('yith-woocommerce-catalog-mode/init.php') && (get_option( 'ywctm_enable_plugin' ) == 'no') ) { ?>
            <span class="woocommerce"><?php ef_get_woo_cart(); ?></span>
            <?php } ?>
            <?php } ?>


            <?php if ( $ef_data['has_map'] && is_page_template( 'templates/contact-template.php' ) ) { ?>
            <!--<div id="ef-expand-map"></div>-->
            <?php } ?>
            <div id="ef-controls-bar" class="hidden-lg hidden-md hidden-sm">
                <a href="#" id="ef-toggle-menu">
                    <span></span>
                </a><!-- #ef-toggle-menu -->
            </div><!-- #ef-controls-bar -->
        </div><!-- #ef-site-name  -->

        <div id="ef-site-nav" class="hidden-lg hidden-md hidden-sm">
            <div id="ef-site-nav-inner">
                <div id="ef-brand">
                    <h1><?php bloginfo( 'name' ); ?></h1>
                    <?php if ( !empty( $ef_data['ef_tagline'] ) ) { ?>
                    <em class="ef-fancy">
                        <?php bloginfo( 'description' ); ?>
                    </em>
                    <?php } ?>
                </div><!-- #ef-brand -->
                <div class="row">
                    <div class="col-lg-6 clearfix">
                        <nav class="top-bar" data-topbar data-options="mobile_show_parent_link: true">
                            <section class="top-bar-section side">
                                <?php ef_primary_navigation(); ?>
                            </section><!-- .top-bar-section -->
                        </nav><!-- .top-bar -->
                    </div><!-- .col-md-12.clearfix -->
                </div><!-- .row -->
            </div><!-- #ef-site-nav-inner -->
            <?php if ( !is_404() && !is_archive() && !post_password_required() && $ef_data['to_support'] && ef_add_share_btns( $ef_data, esc_attr( get_the_title() ), esc_url( get_permalink() ) ) ) { ?>
            <ul id="ef-controls">
                <li id="ef-toggle-share">
                    <span class="ts-icon-share"></span><!-- .icon-ef-share -->
                    <?php echo ef_add_share_btns( $ef_data, esc_attr( get_the_title() ), esc_url( get_permalink() ) ); ?>
                </li><!-- #ef-toggle-share -->
            </ul><!-- #ef-controls -->
            <?php } ?>
        </div><!-- #ef-site-nav -->

        <div id="ef-main-nav" class="hidden-xs">
            <div id="ef-main-nav-inner">
                <div class="row">
                    <div class="col-lg-12 clearfix">
                        <nav class="main main-nav" data-mainbar data-options="mobile_show_parent_link: false">
                            <section class="main main-section superfish">
                                <?php ef_main_navigation(); ?>
                            </section>
                        </nav><!-- .top-bar -->
                    </div><!-- .col-md-12.clearfix -->
                </div><!-- .row -->
            </div><!-- #ef-main-nav-inner -->
            <?php if ( current_theme_supports( 'woocommerce' ) && is_product() ) { ?>
            <div id="ef-header-angle" class="clearfix">
                <span class="ef-header-angle"></span><!-- .ef-header-angle -->
            </div><!-- #ef-header-angle -->
            <?php } ?>
        </div><!-- #ef-main-nav -->
    </nav>

    <div id="ef-tpl-wrapper">
        <header id="ef-header" class="ef-positioner">
            <?php if ( $ef_data['has_map'] && is_page_template( 'templates/contact-template.php' ) ) { ?>
            <?php EfPrintImages::output( $ef_data ); ?>
            <?php } ?>

            <div id="ef-loader">
                <div class="ef-loader-1"></div>

                <div class="ef-loader-2"></div>
            </div><!-- #ef-loader -->
            <?php if ( current_theme_supports( 'woocommerce' ) && is_product() ) { ?>
            <div id="ef-header-inner">
                <?php EfPrintImages::output( $ef_data ); ?>
                <?php if ( !is_single() ) { ?>
                <div id="ef-welcome-block">
                    <?php $author = ef_theme_author_info(); ?>

                    <?php if ( is_front_page() && is_home() ) { ?>
                    <?php ef_get_admin_info(); ?>
                    <?php } else { ?>
                    <h1 id="ef-main-title"><?php ef_page_title( $ef_data ); ?></h1>
                    <?php } ?>

                    <div class="clear"></div>

                    <?php if ( is_author() ) { ?>
                    <?php echo ef_theme_author_info(); ?>
                    <?php } elseif ( is_archive() && term_description() ) { ?>
                    <div id="ef-main-description">
                        <?php echo term_description(); ?>
                    </div>
                    <?php } elseif ( !empty( $ef_data['page_desc'] ) ) { ?>
                    <div id="ef-main-description">
                        <?php echo wp_kses( $ef_data['page_desc'], array( 'a' => array( 'href' => array(), 'title' => array(), 'target' => array() ), 'br' => array(), 'em' => array(), 'span' => array( 'style' => array() ) ) ); ?>
                    </div>
                    <?php } ?>

                    <?php if ( $ef_data['mb_support'] ) { ?>
                    <?php $show_icons = efmb_meta( 'ef_home_icons', 'type=checkbox' ); ?>
                    <?php if ( !empty( $show_icons ) && is_page_template( 'templates/home-template.php' ) ) { ?>
                    <?php ef_add_social_profiles( $ef_data ); ?>
                    <?php } ?>
                    <?php } ?>
                </div><!-- #ef-welcome-block -->
                <?php } ?>
            </div><!-- #ef-header-inner -->

            <?php if ( !( isset( $ef_data['home_style'] ) && $ef_data['home_style'] == 'ef-home-default' ) ) { ?>
            <?php if ( is_page_template( 'templates/home-template.php' ) ) { ?>
            <?php $slCount = count( EfPrintImages::slides( $ef_data ) ); ?>

            <?php if ( $slCount > 0 ) { ?>
            <div id="ef-header-angle" class="ef-bottom-angle clearfix">
                <span class="ef-header-angle"></span>

                <?php if ( $ef_data['home_slideshow'] !== '5' ) { ?>
                <div id="ef-slideshow-nav">
                    <?php if ( $slCount > 1 ) { ?>
                    <ul>
                        <li class="ef-slideshow-prev">
                            <a href="#"></a>
                        </li><!-- .ef-slideshow-prev -->

                        <li class="ef-slideshow-next">
                            <a href="#"></a>
                        </li><!-- .ef-slideshow-next -->
                    </ul>
                    <?php } ?>

                    <?php if ( !empty( $ef_data['parent_link']['url'] ) ) { ?>
                    <a href="<?php echo esc_url( $ef_data['parent_link']['url'] ); ?>"></a>
                    <?php } ?>
                </div><!-- #ef-slideshow-nav -->
                <?php } ?>

                <a id="ef-to-content" href="#">
                    <?php if ( $ef_data['mb_support'] ) { ?>
                    <span><?php echo wp_kses( efmb_meta( 'ef_home_recent_txt', 'type=text' ), array() ) ?></span>
                    <?php } ?>
                </a><!-- #ef-to-content -->
            </div><!-- #ef-header-angle -->
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
        </header><!-- #ef-header -->

        <?php if ( is_page_template( 'templates/home-template.php' ) && !post_password_required() ) { ?>
        <?php EfPrintImages::buildSlider( $ef_data, true ); ?>
        <?php } ?>

        <?php if ( $ef_data['parent_link'] && !is_page_template( 'templates/home-template.php' ) ) { ?>
        <a id="ef-to-parent" href="<?php echo esc_url( $ef_data['parent_link']['url'] ); ?>"></a>
        <?php } ?>

        <?php if ( $ef_data['home_slideshow'] === '5' && !empty( $ef_data['ef_vid_lnk'] ) ) { ?>
        <a id="ef-to-project" class="icon-ef-right-open" href="<?php echo esc_url( $ef_data['ef_vid_lnk'] ); ?>"></a><!-- #ef-to-project -->
        <?php } ?>

        <?php if ( !is_page_template( 'templates/home-template.php' ) ) { ?>
        <section id="ef-content">
            <main id="ef-content-inner" class="clearfix">
                <div id="ef-content-logo" class="row hidden-md hidden-sm hidden-lg">
                    <div class="col-md-12"><?php if ( isset( $ef_data['custom_logo'] ) && !empty( $ef_data['custom_logo'] ) ) { ?>
                        <a class="ef-content-logo1" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $ef_data['custom_logo']; ?>" alt="" /></a>
                        <?php } else { ?>
                        <a class="ef-content-logo1" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri() . '/assets/img/logo.svg'; ?>" alt="" /></a>
                        <?php } ?>
                    </div>
                </div>
                <?php if ( !is_front_page() && !is_home() && !is_product() ) { ?>
                <h1 id="ef-content-title"><?php ef_page_title( $ef_data ); ?></h1>
                <?php } ?>
                <?php } ?>