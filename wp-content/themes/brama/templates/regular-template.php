<?php
/**
 * Template Name: Regular
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

		<?php $ef_data = EfGetOptions::TplConditions(); ?>

		<?php if ( post_password_required() ) { ?>
			<?php echo get_the_password_form(); ?>
		<?php } elseif ( !$ef_data['cpt_support'] ) { ?>
			<?php ef_get_msg_data( $wp_query->query_vars['post_type'], $ef_data ); ?>
		<?php } else { ?>
			<?php if ( $ef_data['cpt_support'] && $ef_data['mb_support'] ) { ?>
				<?php $wp_query_tmp = $wp_query; ?>

				<?php $tax_query = $ef_data['term_slugs'] ? array(
					array(
						'taxonomy'  => $ef_data['team_extr'] ? EF_CPT_EXTR_TAX : EF_CPT_TEAM_TAX,
						'terms'		=> $ef_data['term_slugs'],
						'field'		=> 'slug',
					)
				) : array(); ?>

				<?php $args = array(
					'post_type'				=> $ef_data['team_extr'] ? EF_CPT_EXTR : EF_CPT_TEAM,
					'tax_query'				=> $tax_query,
					'posts_per_page'	  	=> -1,
					'order'					=> 'ASC',
					'post_status'		  	=> 'publish',
					'ignore_sticky_posts' 	=> true
				); ?>

				<?php /* New wp_query for the Extras */ ?>

				<?php $wp_query = NULL; $wp_query = new WP_Query( $args ); ?>

				<?php if ( $ef_data['team_extr'] ) { ?>
					<?php if ( !have_posts() ) { ?>
						<?php ef_get_msg_data( $wp_query->query_vars['post_type'], $ef_data ); ?>
					<?php } else { ?>
						<div id="ef-services">
							<?php while ( have_posts() ) : the_post(); ?>
								<?php $if_has_more = strpos( $post->post_content, '<!--more-->' ); ?>

								<article id="post-<?php the_ID(); ?>" <?php post_class( 'row' ); ?>>
									<?php $icon_extra = efmb_meta( 'ef_extra_icons', 'type=layout' ); ?>
									<div class="col-md-3">
										<?php if ( !empty( $icon_extra ) ) { ?>
											<span class="ef-service-icn <?php echo $icon_extra; ?>"></span>
										<?php } ?>
									</div>

									<div class="col-md-9">
										<header class="clearfix">
											<h1>
												<?php if ( $ef_data['show_content'] !== 'ef-content' || $if_has_more ) { ?>
													<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
												<?php } else { ?>
													<?php the_title(); ?>
												<?php } ?>
											</h1>
										</header>

										<?php if ( $ef_data['show_content'] == 'ef-content' || $if_has_more ) { ?>
											<?php global $more; $more = 0; ?>
											<?php the_content( '', false, '' ); ?>
											<?php $more = 1; ?>
										<?php } else { ?>
											<?php the_excerpt(); ?>
										<?php } ?>
									</div>
								</article>
							<?php endwhile; // end of the loop. ?>
						</div>
					<?php } // have_posts() ?>
				<?php } else { ?>
					<?php if ( !have_posts() ) { ?>
						<?php ef_get_msg_data( $wp_query->query_vars['post_type'], $ef_data ); ?>
					<?php } else { ?>
						<div id="ef-team">
							<?php $l = 1; ?>

							<?php while ( have_posts() ) : the_post(); ?>
								<?php $if_has_more = strpos( $post->post_content, '<!--more-->' ); ?>
								<article id="post-<?php the_ID(); ?>" <?php post_class( 'row' ); ?>>

									<div class="col-md-5<?php echo $l % 2 !== 0 ? ' col-md-push-7' : ' text-right'; ?>">
										<div class="ef-proj-img">
											<?php if ( has_post_thumbnail() ) { ?>
												<?php if ( $ef_data['show_content'] !== 'ef-content' || $if_has_more ) { ?>
													<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute() ?>"><?php echo get_the_post_thumbnail( $post->ID, 'medium_thumb' ); ?></a>
												<?php } else { ?>
													<?php echo get_the_post_thumbnail( $post->ID, 'medium_thumb' ); ?>
												<?php } ?>
												<?php if ( post_password_required() ) echo '<div class="ef-pass ts-icon-lock"></div>'; ?>
											<?php } ?>
										</div>
									</div>

									<div class="col-md-7<?php echo $l % 2 !== 0 ? ' col-md-pull-5' : ''; ?>">
										<header class="clearfix">
											<h1>
												<?php if ( $ef_data['show_content'] !== 'ef-content' || $if_has_more ) { ?>
													<a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute() ?>"><?php the_title(); ?></a>
												<?php } else { ?>
													<?php the_title(); ?>
												<?php } ?>
												<?php $member_pos = efmb_meta( 'ef_member_pos', 'type=text' ); ?>
												<?php if ( !empty( $member_pos ) ) { ?>
													<small class="ef-member-pos"><?php echo wp_kses( $member_pos, array() ); ?></small>
												<?php } ?>
											</h1>
										</header>

										<?php if ( $ef_data['show_content'] == 'ef-content' || $if_has_more ) { ?>
											<?php global $more; $more = 0; ?>
											<?php the_content( '', false, '' ); ?>
											<?php $more = 1; ?>
										<?php } else { ?>
											<?php the_excerpt(); ?>
										<?php } ?>

										<p class="ef-team-social">
											<?php foreach( $ef_data['socialize_team'] as $class => $title ) { ?>
												<?php $ef_icn_url = efmb_meta( 'ef_' . $class, 'type=text', $post->ID ); ?>

												<?php if ( !empty( $ef_icn_url ) ) { ?>
													&middot; <a target="_blank" href="<?php echo strip_tags( esc_url( $ef_icn_url ) ); ?>" title="<?php echo esc_attr( $title ); ?>"><?php echo $title; ?></a>
												<?php } ?>
											<?php } ?>
										</p>
									</div>

									<?php $l++; ?>
								</article>
							<?php endwhile; // end of the loop. ?>
						</div>
					<?php } // have_posts() ?>
				<?php } ?>

				<?php $wp_query = null; $wp_query = $wp_query_tmp; ?>
			<?php } ?>

			<?php get_template_part( 'loop', 'page' ); ?>
		<?php } ?>
<?php get_footer(); ?>