<?php
/**
 * Template Name: Portfolio
 *
 * @package Brama
 * @since Brama 1.0
 */

?>

<?php get_header(); ?>

	<?php if ( post_password_required() ) { ?>
		<?php echo get_the_password_form(); ?>
	<?php } else { ?>
		<?php get_template_part( 'loop', 'page' ); ?>

		<?php $ef_data = EfGetOptions::TplConditions(); ?>
		<?php global $paged; ?>
		<?php $wp_query_tmp = $wp_query; ?>

		<?php $tax_query = $ef_data['term_slugs'] ? array(
			array(
				'taxonomy'  => $ef_data['cpt_support'] ? EF_CPT_TAX_CAT : '',
				'terms'		=> $ef_data['term_slugs'],
				'field'		=> 'slug',
			)
		) : array(); ?>

		<?php $args = array(
			'post_type'				=> $ef_data['cpt_support'] ? EF_CPT : '',
			'tax_query'				=> $tax_query,
			'posts_per_page'	  	=> isset( $ef_data['posts_per'] ) ? $ef_data['posts_per'] : -1,
			'post_status'		  	=> 'publish',
			'ignore_sticky_posts' 	=> true,
			'meta_key' 			  	=> '_thumbnail_id',
			'paged'				  	=> $paged
		); ?>

		<?php $wp_query = NULL; $wp_query = new WP_Query( $args ); ?>

		<?php if ( $ef_data['cpt_support'] && $wp_query->found_posts > 0 ) { ?>
			<?php if ( ( isset( $ef_data['paging_type'] ) && $ef_data['paging_type'] == 'regular' ) || get_query_var('paged') ) { ?>
				<?php $post_ids_array = wp_list_pluck( $wp_query->posts, 'ID' ); ?>
				<?php $p_terms = wp_get_object_terms( $post_ids_array, EF_CPT_TAX_CAT ); ?>
			<?php } else { ?>
				<?php $p_terms = get_terms( EF_CPT_TAX_CAT, array( 'orderby' => 'slug', 'include' => $ef_data['term_ids'], 'hierarchical' => false ) ); ?>
			<?php } ?>

			<?php $terms_array = array(); $id = $cat = null; ?>
			<?php foreach ( $p_terms as $term ) { ?>
				<?php $terms_array[$term->term_id] = array( 'name' => $term->name ); ?>
			<?php } ?>

			<?php if ( count( $terms_array ) > 0 ) { ?>
				<div class="row">
	                <div class="col-md-12">
	                    <ul id="ef-portfolio-filter" class="list-inline text-center">
	                    	<?php if ( count( $terms_array ) > 1 ) { ?>
		                    	<li class="cbp-filter-item-active"><a href="#" data-filter="*"><?php _e( 'All', EF_TDM ) ?>
		                                <span class="cbp-filter-counter"></span>
		                            </a>
		                        </li>
	                        <?php } ?>

	                        <?php $cur = count( $terms_array ) == 1 ? ' class="cbp-filter-item-active"' : ''; ?>

							<?php foreach ( $terms_array as $id => $cat ) { ?>
								<?php echo '<li' . $cur . '><a data-filter=".cbp-filter-' . $id . '" href="' . esc_url( get_term_link( $id, EF_CPT_TAX_CAT ) ) . '">' . $cat['name'] . '<span class="cbp-filter-counter"></span></a></li>'; ?>
							<?php } ?>
						</ul><!-- #ef-portfolio-filter -->
					</div><!-- .col-md-12 -->
				</div><!-- .row -->
			<?php } ?>
		<?php } ?>

		<?php get_template_part( 'loop', 'portfolio' ); ?>

		<?php /* Pagination */ ?>
		<?php ef_theme_pagination( $wp_query->max_num_pages, $ef_data ); ?>

		<?php $wp_query = null; $wp_query = $wp_query_tmp; ?>
	<?php } ?>

<?php get_footer(); ?>